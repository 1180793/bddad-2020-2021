-- PL08
-- A. Cláusulas Join
-- 1. Obter o produto cartesiano (i.e. CROSS JOIN) entre as tabelas Empregado e EmpregadoEfetivo, de acordo com a Figura 2. 
SELECT e.idEmpregado, ef.idEmpregado
FROM EMPREGADO e
CROSS JOIN EMPREGADOEFETIVO ef
ORDER BY e.idEmpregado, ef.idEmpregado;

-- 2. Obter toda a informação dos empregados efetivos, por ordem alfabética dos nomes e de acordo com a Figura 3. 
SELECT e.idEmpregado, e.nome, e.datanascimento, e.nrIdentificacaoCivil, e.nif, ef.salarioMensalBase
FROM EMPREGADOEFETIVO ef
INNER JOIN EMPREGADO e
ON (ef.idEmpregado = e.idEmpregado)
ORDER BY e.NOME;

-- 3. Obter as faltas dos empregados efetivos, de acordo com a Figura 4.
SELECT f.*
FROM FALTA f
INNER JOIN EMPREGADOEFETIVO ef
ON (ef.idEmpregado = f.idEmpregado);

-- 4. Obter as faltas dos empregados temporários, de acordo com a Figura 5.
SELECT e.nome, e.nrIdentificacaoCivil, e.idEmpregado, f.dataInicio, f.dataFim, f.justificacao
FROM FALTA f
INNER JOIN EMPREGADOTEMPORARIO et
    ON (et.idEmpregado = f.idEmpregado)
INNER JOIN EMPREGADO e
    ON (e.idEmpregado = f.idEmpregado);

-- 5. Obter as avaliações dos empregados temporários, de acordo com a Figura 6.
SELECT e.nome, e.nrIdentificacaoCivil, at.idDepartamento, ed.dataInicio, ed.dataFim, a.descricao
FROM AVALIACAOTEMPORARIO at
INNER JOIN EMPREGADO e
    ON (e.idEmpregado = at.idEmpregado)
INNER JOIN AVALIACAO a
    ON (at.idAvaliacao = a.idAvaliacao)
INNER JOIN EMPREGADODEPARTAMENTO ed
    ON (ed.dataInicio = at.dataInicio AND at.idEmpregado = ed.idEmpregado);

-- 6. Obter o nome e o número de identificação civil de todos empregados que nunca faltaram, de acordo com a Figura 7.
-- Obs: Inverse INNER JOIN
SELECT e.nome, e.nrIdentificacaoCivil
FROM EMPREGADO e
LEFT JOIN FALTA f
    ON (e.idEmpregado = f.idEmpregado)
WHERE f.idEmpregado IS NULL
ORDER BY e.nrIdentificacaoCivil;

-- 7. Obter o identificador e a designação de todos os departamentos juntamente com a designação do respetivo departamento do 
--   nível hierárquico superior. O resultado apresentado deve estar de acordo com a Figura 8.
SELECT d.idDepartamento, d.designacao, NVL(TO_CHAR(d1.designacao), ' ' ) AS DEPARTAMENTO_NIVEL_SUPERIOR
FROM DEPARTAMENTO d
LEFT OUTER JOIN DEPARTAMENTO d1
    ON (d.idDepartamentoSuperior = d1.idDepartamento)
ORDER BY d1.designacao DESC;
    
-- 8. Obter pares de empregados distintos e com idades diferentes, de acordo com a Figura 9. 
SELECT e1.idEmpregado, e1.nome, e2.idEmpregado, e2.nome
FROM EMPREGADO e1
CROSS JOIN EMPREGADO e2
WHERE (TRUNC(MONTHS_BETWEEN(SYSDATE, e1.dataNascimento)/12) < TRUNC(MONTHS_BETWEEN(SYSDATE, e2.dataNascimento)/12))
ORDER BY e1.idEmpregado, e2.idEmpregado;

-- B. Operadores de Conjuntos com Cláusulas Join
-- 1. Obter o nome e o número de identificação civil de todos os empregados que tiveram pelo menos uma avaliação "MUITO BOM". 
--   O resultado apresentado deve estar de acordo com a Figura 10.
SELECT DISTINCT e.nome, e.nrIdentificacaoCivil
FROM AVALIACAOTEMPORARIO at
INNER JOIN EMPREGADO e
    ON (e.idEmpregado = at.idEmpregado)
INNER JOIN AVALIACAO a
    ON (at.idAvaliacao = a.idAvaliacao)
WHERE (a.descricao = 'MUITO BOM')
UNION
SELECT DISTINCT e.nome, e.nrIdentificacaoCivil
FROM AVALIACAOEFETIVO ae
INNER JOIN EMPREGADO e
    ON (e.idEmpregado = ae.idEmpregado)
INNER JOIN AVALIACAO a
    ON (ae.idAvaliacao = a.idAvaliacao)
WHERE (a.descricao = 'MUITO BOM');

-- 2. Obter a descrição das avaliações que são comuns a empregados efetivos e temporários, de acordo com a Figura 11.
SELECT DISTINCT a.descricao
FROM AVALIACAOEFETIVO af
INNER JOIN AVALIACAO a
    ON (af.idAvaliacao = a.idAvaliacao)
INTERSECT
SELECT DISTINCT a.descricao
FROM AVALIACAOTEMPORARIO at
INNER JOIN AVALIACAO a
    ON (at.idAvaliacao = a.idAvaliacao);

-- 3. Obter o nome e o número de identificação civil dos empregados efetivos que tiveram sempre classificação "MUITO BOM", 
--   de acordo com a Figura 12.
SELECT DISTINCT e.nome, e.nrIdentificacaoCivil
FROM AVALIACAOEFETIVO af
INNER JOIN EMPREGADO e
    ON (af.idEmpregado = e.idEmpregado)
INNER JOIN AVALIACAO a
    ON (af.idAvaliacao = a.idAvaliacao)
WHERE (a.descricao = 'MUITO BOM')
MINUS
SELECT DISTINCT e.nome, e.nrIdentificacaoCivil
FROM AVALIACAOEFETIVO af
INNER JOIN EMPREGADO e
    ON (af.idEmpregado = e.idEmpregado)
INNER JOIN AVALIACAO a
    ON (af.idAvaliacao = a.idAvaliacao)
WHERE (a.descricao != 'MUITO BOM');

-- 4. Obter o nome e o número de identificação civil dos empregados efetivos cujas férias têm todas duração superior a 15 dias. 
--   O resultado apresentado deve estar de acordo com a Figura 13.
SELECT DISTINCT e.nome, e.nrIdentificacaoCivil
FROM FERIAS f
INNER JOIN EMPREGADOEFETIVO ee
    ON (ee.idEmpregado = f.idEmpregado)
INNER JOIN EMPREGADO e
    ON (e.idEmpregado = f.idEmpregado)
WHERE (TRUNC(f.dataFim - f.dataInicio) > 15)
MINUS
SELECT DISTINCT e.nome, e.nrIdentificacaoCivil
FROM FERIAS f
INNER JOIN EMPREGADOEFETIVO ee
    ON (ee.idEmpregado = f.idEmpregado)
INNER JOIN EMPREGADO e
    ON (e.idEmpregado = f.idEmpregado)
WHERE (TRUNC(f.dataFim - f.dataInicio) <= 15);