-- PL12
-- 1. Mostrar o nome dos médicos que receitaram mais de três medicamentos em cada uma das suas consultas (Figura 2). Considerar
--  apenas médicos que tenham mais do que uma consulta. O resultado deve ser apresentado por ordem alfabética do nome dos médicos.
SELECT DISTINCT m.nome
FROM MEDICO m 
INNER JOIN CONSULTA c1 
ON m.idMedico = c1.idMedico 
WHERE 3 <= ALL(
            SELECT (
                SELECT COUNT(*) 
                FROM MEDICAMENTORECEITADO mcr
                WHERE mcr.idConsulta = c2.idConsulta 
            )
            FROM CONSULTA c2                        
            WHERE c2.idMedico = m.idMedico 
)
ORDER BY 1;

-- 2. Mostrar o nome dos médicos, que receitaram sempre o mesmo número de medicamentos em todas as suas consultas, juntamente 
--  com esse número (Figura 3). O resultado deve ser apresentado por ordem alfabética do nome dos médicos.
WITH MEDICAMENTOS_RECEITADOS_CONSULTA AS
(
    SELECT 
        idConsulta
        , COUNT(idMedicamento) AS nrMedicamentosReceitados
        , (
            SELECT idMedico
            FROM CONSULTA c
            WHERE c.idConsulta = mr.idConsulta 
        ) AS idMedico
    FROM MEDICAMENTORECEITADO mr
    GROUP BY idConsulta 
)
SELECT m.nome, MAX(mrc.nrMedicamentosReceitados) AS nrMedicamentosReceitados
FROM MEDICO m
INNER JOIN MEDICAMENTOS_RECEITADOS_CONSULTA mrc 
ON m.idMedico = mrc.idMedico
GROUP BY m.nome
HAVING MAX(mrc.nrMedicamentosReceitados) = MIN(mrc.nrMedicamentosReceitados) AND COUNT(*) > 1
ORDER BY 1;
