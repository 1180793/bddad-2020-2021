-- PL10
-- 1. Mostrar o id, o nome e a classificação dos marinheiros com a melhor classificação (Figura 2), usando três estratégias 
--   diferentes, nomeadamente:
-- a) Sem junção de tabelas e com função de agregação;
SELECT m.idMarinheiro, m.nome, m.classificacao
FROM MARINHEIRO m
WHERE m.classificacao >= (SELECT MAX(m2.classificacao) FROM MARINHEIRO m2);

-- b) Sem junção de tabelas e sem função de agregação;
SELECT m.idMarinheiro, m.nome, m.classificacao
FROM MARINHEIRO m
WHERE m.classificacao >= ALL(SELECT m2.classificacao FROM MARINHEIRO m2);

-- c) Com junção de tabelas.
SELECT m1.idMarinheiro, m1.nome, m1.classificacao
FROM MARINHEIRO m1, (SELECT MAX(m2.classificacao) classificacao FROM MARINHEIRO m2) c
WHERE m1.classificacao = c.classificacao;

-- d) Usando apenas os operadores básicos da teoria de conjuntos. M1 x M2 Produto Cartesiano
SELECT m1.idMarinheiro, m1.nome, m1.classificacao
FROM MARINHEIRO m1
WHERE m1.classificacao 
IN (
    SELECT DISTINCT m2.classificacao
    FROM MARINHEIRO m2
    WHERE m2.classificacao NOT IN (
        SELECT DISTINCT a.classificacao
        FROM MARINHEIRO a, MARINHEIRO b
        WHERE a.classificacao < b.classificacao
    )
);

-- 2. Mostrar o id, o nome, a classificação dos marinheiros e o comentário “Superior à média” associado apenas aos marinheiros
--   que têm classificação superior à média (Figura 3). Resolver usando três estratégias diferentes, nomeadamente:
-- a) Sem junção de tabelas;
SELECT m1.idMarinheiro, m1.nome, m1.classificacao, 
    CASE WHEN m1.classificacao > (SELECT AVG(classificacao) FROM MARINHEIRO)
        THEN 'Superior à média'
    ELSE
        ' '
    END obs
FROM MARINHEIRO m1;

-- b) Com junção de tabelas e sem cláusula WITH;
SELECT m1.idMarinheiro, m1.nome, m1.classificacao, 
    CASE WHEN a.media IS NOT NULL
        THEN 'Superior à média'
    ELSE
        ' '
    END obs
FROM MARINHEIRO m1
LEFT JOIN (SELECT AVG(classificacao) media FROM marinheiro) a
ON m1.classificacao > a.media;

-- c) Com junção de tabelas e com cláusula WITH.
WITH a AS (SELECT AVG(classificacao) media FROM marinheiro)
SELECT m1.idMarinheiro, m1.nome, m1.classificacao, 
    CASE WHEN a.media IS NOT NULL 
        THEN 'Superior à média'
    ELSE
        ' '
    END obs
FROM MARINHEIRO m1
LEFT JOIN a
ON m1.classificacao > a.media;

-- 3. Mostrar o id e o nome dos marinheiros que não reservaram barcos (Figura 4), usando quatro estratégias diferentes, 
--   nomeadamente: 
-- a) Com operador IN;
SELECT m1.idMarinheiro, m1.nome
FROM MARINHEIRO m1
WHERE m1.idMarinheiro NOT IN (SELECT idMarinheiro FROM RESERVA)
ORDER BY 1;

-- b) Com condição ANY;
SELECT m1.idMarinheiro, m1.nome
FROM MARINHEIRO m1
WHERE m1.idMarinheiro NOT IN (
    SELECT idMarinheiro
    FROM MARINHEIRO
    WHERE idMarinheiro = ANY (
        SELECT idMarinheiro 
        FROM RESERVA
        )
    )
ORDER BY 1;        

-- c) Com operador de conjuntos;
SELECT a.idMarinheiro, nome
FROM(
    SELECT m.idMarinheiro
    FROM MARINHEIRO m
    MINUS
    SELECT r.idMarinheiro
    FROM RESERVA r
) a
INNER JOIN MARINHEIRO b
ON (a.idMarinheiro = b.idMarinheiro)
ORDER BY 1;

-- d) Sem subqueries.
SELECT m1.idMarinheiro, m1.nome
FROM MARINHEIRO m1
MINUS
SELECT m2.idMarinheiro, m2.nome
FROM MARINHEIRO m2, RESERVA r
WHERE m2.idMarinheiro = r.idMarinheiro;

-- 4. Mostrar as datas com mais reservas de barcos (Figura 5).
SELECT r1.data 
FROM reserva r1
GROUP BY r1.data
HAVING COUNT(*) = (
    SELECT MAX(COUNT(r2.data))
    FROM reserva r2 
    GROUP BY r2.data
);

-- 5. Mostrar o número total de marinheiros que reservaram barcos com a cor vermelho e barcos com a cor verde (Figura 6).
SELECT COUNT(*) AS QTD_MARINHEIROS 
FROM
(
    ( -- Lista de Marinheiros que reservaram barcos da cor 'VERMELHO'
        SELECT r.idMarinheiro 
        FROM RESERVA r
        INNER JOIN BARCO b 
        ON b.idBarco = r.idBarco
        WHERE UPPER(b.cor) = 'VERMELHO'
    )
    INTERSECT -- A interseção dá-nos os idMarinheiro dos marinheiros que reservaram barcos de ambas as cores.
    ( -- Lista de Marinheiros que reservaram barcos da cor 'VERDE'
        SELECT r1.idMarinheiro 
        FROM RESERVA r1
        INNER JOIN BARCO b1 
        ON b1.idBarco = r1.idBarco
        WHERE UPPER(b1.cor) = 'VERDE'
    )
);