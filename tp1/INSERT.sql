------------------------------------------------------------------------------------------------------
-- Inserir Registros
------------------------------------------------------------------------------------------------------

--  CLIENTES nif INTEGER, nome VARCHAR(50), email VARCHAR(50), telefone  VARCHAR(15), localidade VARCHAR(15), concelho VARCHAR(50)
--  3 registos de clientes por zona do pa�s (Norte, Centro, Sul)

INSERT INTO Cliente VALUES (222222001, 'Joao', 'joao@gmail.com', '+351919191919', 'Norte', 'Porto');
INSERT INTO Cliente VALUES (222222002, 'Jose Silva', 'josesilva@gmail.com', '+351911261452', 'Norte', 'Vila Real');
INSERT INTO Cliente VALUES (222222003, 'Andre', 'andre@gmail.com', '+351918265923', 'Norte', 'Porto');
INSERT INTO Cliente VALUES (222222004, 'Vitor', 'vitor@gmail.com', '+351919472839', 'Norte', 'Porto');
INSERT INTO Cliente VALUES (222222005, 'Joana', 'joana@gmail.com', '+351919174816', 'Norte', 'Porto');
INSERT INTO Cliente VALUES (222222006, 'Catia', 'catia@gmail.com', '+351910925381', 'Norte', 'Porto');
INSERT INTO Cliente VALUES (222222007, 'Sandra', 'sandra@gmail.com', '+351911762539', 'Centro', 'Lisboa');
INSERT INTO Cliente VALUES (222222008, 'Manuel', 'manuel@gmail.com', '+351910176525', 'Centro', 'Lisboa');
INSERT INTO Cliente VALUES (222222009, 'Pedro', 'pedro@gmail.com', '+351911524631', 'Centro', 'Lisboa');
INSERT INTO Cliente VALUES (222222010, 'Miguel', 'miguel@gmail.com', '+351918271521', 'Centro', 'Lisboa');
INSERT INTO Cliente VALUES (222222011, 'Bruno', 'bruno@gmail.com', '+351911827212', 'Centro', 'Lisboa');
INSERT INTO Cliente VALUES (222222012, 'Manuela', 'manuela@gmail.com', '+351911313457', 'Sul', 'Faro');
INSERT INTO Cliente VALUES (222222013, 'Barbara', 'barbara@gmail.com', '+351919387585', 'Sul', 'Faro');
INSERT INTO Cliente VALUES (222222014, 'Sara', 'sara@gmail.com', '+351913928764', 'Sul', 'Faro');
INSERT INTO Cliente VALUES (222222015, 'Goncalo', 'goncalo@gmail.com', '+351914463788', 'Sul', 'Faro');
INSERT INTO Cliente VALUES (222222016, 'Antonio', 'antonio@gmail.com', '+351910299972', 'Sul', 'Faro');


--  ANDAR  idAndar INTEGER, nome VARCHAR(255)

INSERT INTO Andar VALUES (1, 'Primeiro Andar');
INSERT INTO Andar VALUES (2, 'Segundo Andar');


--  TIPOQUARTO idTipoQuarto INTEGER, descricao VARCHAR(50)

INSERT INTO TipoQuarto VALUES (1, 'Suite');
INSERT INTO TipoQuarto VALUES (2, 'Single');
INSERT INTO TipoQuarto VALUES (3, 'Superior');
INSERT INTO TipoQuarto VALUES (4, 'Twin');

--  QUARTO  idQuarto INTEGER, idAndar INTEGER, idTipoQuarto INTEGER, lotacao INTEGER, 
--  10 registos de quartos de cada tipo em cada andar; 

-- Primeiro Andar
INSERT INTO Quarto VALUES (101, 1, 1, 4);
INSERT INTO Quarto VALUES (102, 1, 2, 1);
INSERT INTO Quarto VALUES (103, 1, 3, 5);
INSERT INTO Quarto VALUES (104, 1, 4, 2);
INSERT INTO Quarto VALUES (105, 1, 1, 4);
INSERT INTO Quarto VALUES (106, 1, 2, 1);
INSERT INTO Quarto VALUES (107, 1, 3, 5);
INSERT INTO Quarto VALUES (108, 1, 4, 2);
INSERT INTO Quarto VALUES (109, 1, 1, 4);
INSERT INTO Quarto VALUES (110, 1, 2, 1);
-- Segundo Andar
INSERT INTO Quarto VALUES (201, 2, 1, 4);
INSERT INTO Quarto VALUES (202, 2, 2, 1);
INSERT INTO Quarto VALUES (203, 2, 3, 5);
INSERT INTO Quarto VALUES (204, 2, 4, 2);
INSERT INTO Quarto VALUES (205, 2, 1, 4);
INSERT INTO Quarto VALUES (206, 2, 2, 2);
INSERT INTO Quarto VALUES (207, 2, 3, 5);
INSERT INTO Quarto VALUES (208, 2, 4, 2);
INSERT INTO Quarto VALUES (209, 2, 1, 4);
INSERT INTO Quarto VALUES (210, 2, 2, 1);


--  RESERVAS  nifCliente INTEGER, idQuarto INTEGER, idAndar INTEGER, dataInicio DATE, dataFim DATE, qtdPessoas INTEGER, estado VARCHAR(15), epoca VARCHAR(10), preco NUMERIC(7,2)
--  5 registos de reservas por �poca do ano
--  �poca M�dia - De 01/04 a 15/06  e de 16/09 a 31/10  
--  �poca Baixa - De 01/11 a 31/03
--  �poca Alta - De 16/06 a 15/09 

INSERT INTO Reserva (nifCliente, idQuarto, idAndar, dataInicio, dataFim, qtdPessoas, estado, epoca, preco) VALUES (222222001, 101, 1, TO_DATE('18/11/2020', 'dd/mm/yyyy'), TO_DATE('19/11/2020', 'dd/mm/yyyy'), 2, 'Reservada', 'Baixa', 100.0); 
INSERT INTO Reserva (nifCliente, idQuarto, idAndar, dataInicio, dataFim, qtdPessoas, estado, epoca, preco) VALUES (222222002, 102, 1, TO_DATE('18/03/2020', 'dd/mm/yyyy'), TO_DATE('19/03/2020', 'dd/mm/yyyy'), 1, 'Reservada', 'Baixa', 100.0);
INSERT INTO Reserva (nifCliente, idQuarto, idAndar, dataInicio, dataFim, qtdPessoas, estado, epoca, preco) VALUES (222222003, 103, 1, TO_DATE('21/03/2020', 'dd/mm/yyyy'), TO_DATE('30/03/2020', 'dd/mm/yyyy'), 4, 'Reservada', 'Baixa', 100.0);
INSERT INTO Reserva (nifCliente, idQuarto, idAndar, dataInicio, dataFim, qtdPessoas, estado, epoca, preco) VALUES (222222004, 104, 1, TO_DATE('10/08/2020', 'dd/mm/yyyy'), TO_DATE('15/08/2020', 'dd/mm/yyyy'), 2, 'Reservada', 'Alta', 100.0);

INSERT INTO Reserva (nifCliente, idQuarto, idAndar, dataInicio, dataFim, qtdPessoas, estado, epoca, preco) VALUES (222222005, 201, 2, TO_DATE('17/06/2020', 'dd/mm/yyyy'), TO_DATE('20/06/2020', 'dd/mm/yyyy'), 2, 'Reservada', 'Alta', 100.0);
INSERT INTO Reserva (nifCliente, idQuarto, idAndar, dataInicio, dataFim, qtdPessoas, estado, epoca, preco) VALUES (222222006, 202, 2, TO_DATE('15/08/2020', 'dd/mm/yyyy'), TO_DATE('16/08/2020', 'dd/mm/yyyy'), 1, 'Reservada', 'Alta', 100.0);
INSERT INTO Reserva (nifCliente, idQuarto, idAndar, dataInicio, dataFim, qtdPessoas, estado, epoca, preco) VALUES (222222007, 203, 2, TO_DATE('10/09/2020', 'dd/mm/yyyy'), TO_DATE('15/09/2020', 'dd/mm/yyyy'), 3, 'Reservada', 'Alta', 100.0);
INSERT INTO Reserva (nifCliente, idQuarto, idAndar, dataInicio, dataFim, qtdPessoas, estado, epoca, preco) VALUES (222222008, 204, 2, TO_DATE('10/09/2020', 'dd/mm/yyyy'), TO_DATE('15/09/2020', 'dd/mm/yyyy'), 2, 'Reservada', 'Alta', 100.0);

INSERT INTO Reserva (nifCliente, idQuarto, idAndar, dataInicio, dataFim, qtdPessoas, estado, epoca, preco) VALUES (222222009, 105, 1, TO_DATE('12/02/2020', 'dd/mm/yyyy'), TO_DATE('20/02/2020', 'dd/mm/yyyy'), 1, 'Reservada', 'Baixa', 100.0);
INSERT INTO Reserva (nifCliente, idQuarto, idAndar, dataInicio, dataFim, qtdPessoas, estado, epoca, preco) VALUES (222222010, 106, 1, TO_DATE('12/12/2020', 'dd/mm/yyyy'), TO_DATE('18/12/2020', 'dd/mm/yyyy'), 1, 'Reservada', 'Baixa', 100.0);
INSERT INTO Reserva (nifCliente, idQuarto, idAndar, dataInicio, dataFim, qtdPessoas, estado, epoca, preco) VALUES (222222011, 107, 1, TO_DATE('12/04/2020', 'dd/mm/yyyy'), TO_DATE('16/04/2020', 'dd/mm/yyyy'), 5, 'Reservada', 'Media', 100.0);
INSERT INTO Reserva (nifCliente, idQuarto, idAndar, dataInicio, dataFim, qtdPessoas, estado, epoca, preco) VALUES (222222012, 108, 1, TO_DATE('15/02/2020', 'dd/mm/yyyy'), TO_DATE('16/05/2020', 'dd/mm/yyyy'), 2, 'Reservada', 'Media', 100.0);


INSERT INTO Reserva (nifCliente, idQuarto, idAndar, dataInicio, dataFim, qtdPessoas, estado, epoca, preco) VALUES (222222013, 205, 2, TO_DATE('16/09/2020', 'dd/mm/yyyy'), TO_DATE('22/09/2020', 'dd/mm/yyyy'), 4, 'Reservada', 'Media', 100.0);
INSERT INTO Reserva (nifCliente, idQuarto, idAndar, dataInicio, dataFim, qtdPessoas, estado, epoca, preco) VALUES (222222014, 206, 2, TO_DATE('16/09/2020', 'dd/mm/yyyy'), TO_DATE('22/09/2020', 'dd/mm/yyyy'), 1, 'Reservada', 'Media', 100.0);
INSERT INTO Reserva (nifCliente, idQuarto, idAndar, dataInicio, dataFim, qtdPessoas, estado, epoca, preco) VALUES (222222015, 207, 2, TO_DATE('16/09/2020', 'dd/mm/yyyy'), TO_DATE('22/09/2020', 'dd/mm/yyyy'), 4, 'Reservada', 'Media', 100.0);
INSERT INTO Reserva (nifCliente, idQuarto, idAndar, dataInicio, dataFim, qtdPessoas, estado, epoca, preco) VALUES (222222016, 208, 2, TO_DATE('16/09/2020', 'dd/mm/yyyy'), TO_DATE('22/09/2020', 'dd/mm/yyyy'), 2, 'Reservada', 'Media', 100.0);


--  FUNCIONARIOS  nif INTEGER, nome VARCHAR(50), email VARCHAR(50), telefone VARCHAR(15), morada VARCHAR(255)

INSERT INTO Funcionario VALUES (333333123, 'Carla', 'carla_camareira@gmail.com', '+351929292929', 'Casa_1');
INSERT INTO Funcionario VALUES (333333567, 'Ana', 'ana_camareira@gmail.com', '+351939393939', 'Casa_2');
INSERT INTO Funcionario VALUES (333333142, 'Maria', 'maria_camareira@gmail.com', '+351949494949', 'Casa_3');
INSERT INTO Funcionario VALUES (333333652, 'Beatriz', 'beatriz_camareira@gmail.com', '+351959595959', 'Casa_4');
INSERT INTO Funcionario VALUES (333333834, 'Filipa', 'filipa_camareira@gmail.com', '+351969696969', 'Casa_5');

--  CAMAREIRAS nif INTEGER
--  5 registos de camareiras

INSERT INTO Camareira VALUES (333333123);
INSERT INTO Camareira VALUES (333333567);
INSERT INTO Camareira VALUES (333333142);
INSERT INTO Camareira VALUES (333333652);
INSERT INTO Camareira VALUES (333333834);

-- PRODUTOS idProduto INTEGER nome VARCHAR(25) tipoProduto VARCHAR(15)	preco NUMERIC(7,2)
--  10 registos de bebidas e 7 registos de snacks no mini-bar; 
INSERT INTO Produto (idProduto, nome, tipoProduto, preco) VALUES (1, 'Agua 1.5L', 'Bebida', 50.0);
INSERT INTO Produto (idProduto, nome, tipoProduto, preco) VALUES (2, 'Ice Tea Manga', 'Bebida', 50.0);
INSERT INTO Produto (idProduto, nome, tipoProduto, preco) VALUES (3, 'Vodka', 'Bebida', 50.0);
INSERT INTO Produto (idProduto, nome, tipoProduto, preco) VALUES (4, 'Compal Manga Laranja', 'Bebida', 50.0);
INSERT INTO Produto (idProduto, nome, tipoProduto, preco) VALUES (5, 'Coca Cola', 'Bebida', 50.0);
INSERT INTO Produto (idProduto, nome, tipoProduto, preco) VALUES (6, 'Gin', 'Bebida', 50.0);
INSERT INTO Produto (idProduto, nome, tipoProduto, preco) VALUES (7, 'Whiskey', 'Bebida', 50.0);
INSERT INTO Produto (idProduto, nome, tipoProduto, preco) VALUES (8, 'Cerveja (Mini)', 'Bebida', 50.0);
INSERT INTO Produto (idProduto, nome, tipoProduto, preco) VALUES (9, 'Tequila', 'Bebida', 50.0);
INSERT INTO Produto (idProduto, nome, tipoProduto, preco) VALUES (10, 'Frize Lim�o', 'Bebida', 50.0);

INSERT INTO Produto (idProduto, nome, tipoProduto, preco) VALUES (11, 'KitKat', 'Snack', 10.0);
INSERT INTO Produto (idProduto, nome, tipoProduto, preco) VALUES (12, 'Twix', 'Snack', 10.50);
INSERT INTO Produto (idProduto, nome, tipoProduto, preco) VALUES (13, 'Oreo', 'Snack', 10.50);
INSERT INTO Produto (idProduto, nome, tipoProduto, preco) VALUES (14, 'Lion', 'Snack', 10.50);
INSERT INTO Produto (idProduto, nome, tipoProduto, preco) VALUES (15, 'Mars', 'Snack', 10.50);
INSERT INTO Produto (idProduto, nome, tipoProduto, preco) VALUES (16, 'Milka', 'Snack', 10.50);
INSERT INTO Produto (idProduto, nome, tipoProduto, preco) VALUES (17, 'Bounty', 'Snack', 50.50);

--  CONTAS   idConta INTEGER, dataAbertura DATE, nifCliente INTEGER
--  CONSUMOS idConsumo INTEGER,  idConta INTEGER, dataAbertura DATE,  nifCamareira INTEGER, produto VARCHAR(30), precoConsumo NUMERIC(7,2);
--  20 registos de clientes que consumiram nos �ltimos 6 meses (de 21/5 a 21/11) e 5 registos de clientes que consumiram fora desses 6 meses. 

INSERT INTO Conta (dataAbertura, nifCliente) VALUES (TO_DATE('18/11/2020', 'dd/mm/yyyy'), 222222001);
INSERT INTO Consumo (idConta, dataAberturaConta, nifCamareira, idProduto)VALUES (1, TO_DATE('18/11/2020', 'dd/mm/yyyy'), 333333123, 1);
INSERT INTO Consumo (idConta, dataAberturaConta, nifCamareira, idProduto)VALUES (1, TO_DATE('18/11/2020', 'dd/mm/yyyy'), 333333123, 2);
INSERT INTO Consumo (idConta, dataAberturaConta, nifCamareira, idProduto)VALUES (1, TO_DATE('18/11/2020', 'dd/mm/yyyy'), 333333123, 3);

INSERT INTO Conta (dataAbertura, nifCliente) VALUES (TO_DATE('18/03/2020', 'dd/mm/yyyy'), 222222002);
INSERT INTO Consumo (idConta, dataAberturaConta, nifCamareira, idProduto)VALUES (2, TO_DATE('18/03/2020', 'dd/mm/yyyy'), 333333567, 4); -- Fora dos ultimos 6 meses
INSERT INTO Consumo (idConta, dataAberturaConta, nifCamareira, idProduto)VALUES (2, TO_DATE('18/03/2020', 'dd/mm/yyyy'), 333333567, 5); -- Fora dos ultimos 6 meses

INSERT INTO Conta (dataAbertura, nifCliente) VALUES (TO_DATE('21/03/2020', 'dd/mm/yyyy'), 222222003);
INSERT INTO Consumo (idConta, dataAberturaConta, nifCamareira, idProduto)VALUES (3, TO_DATE('21/03/2020', 'dd/mm/yyyy'), 333333142, 6); -- Fora dos ultimos 6 meses

INSERT INTO Conta (dataAbertura, nifCliente) VALUES (TO_DATE('10/08/2020', 'dd/mm/yyyy'), 222222004);
INSERT INTO Consumo (idConta, dataAberturaConta, nifCamareira, idProduto)VALUES (4, TO_DATE('10/08/2020', 'dd/mm/yyyy'), 333333142, 7);

INSERT INTO Conta (dataAbertura, nifCliente) VALUES (TO_DATE('17/06/2020', 'dd/mm/yyyy'), 222222005);
INSERT INTO Consumo (idConta, dataAberturaConta, nifCamareira, idProduto)VALUES (5, TO_DATE('17/06/2020', 'dd/mm/yyyy'), 333333142, 8);
INSERT INTO Consumo (idConta, dataAberturaConta, nifCamareira, idProduto)VALUES (5, TO_DATE('17/06/2020', 'dd/mm/yyyy'), 333333142, 11);

INSERT INTO Conta (dataAbertura, nifCliente) VALUES (TO_DATE('15/08/2020', 'dd/mm/yyyy'), 222222006);
INSERT INTO Consumo (idConta, dataAberturaConta, nifCamareira, idProduto)VALUES (6, TO_DATE('15/08/2020', 'dd/mm/yyyy'), 333333142, 9);
INSERT INTO Consumo (idConta, dataAberturaConta, nifCamareira, idProduto)VALUES (6, TO_DATE('15/08/2020', 'dd/mm/yyyy'), 333333142, 10);

INSERT INTO Conta (dataAbertura, nifCliente) VALUES (TO_DATE('10/09/2020', 'dd/mm/yyyy'), 222222007);
INSERT INTO Consumo (idConta, dataAberturaConta, nifCamareira, idProduto)VALUES (7, TO_DATE('10/09/2020', 'dd/mm/yyyy'), 333333142, 11);

INSERT INTO Conta (dataAbertura, nifCliente) VALUES (TO_DATE('10/09/2020', 'dd/mm/yyyy'), 222222008);
INSERT INTO Consumo (idConta, dataAberturaConta, nifCamareira, idProduto)VALUES (8, TO_DATE('10/09/2020', 'dd/mm/yyyy'), 333333142, 12);
INSERT INTO Consumo (idConta, dataAberturaConta, nifCamareira, idProduto)VALUES (8, TO_DATE('10/09/2020', 'dd/mm/yyyy'), 333333142, 13);

INSERT INTO Conta (dataAbertura, nifCliente) VALUES (TO_DATE('12/02/2020', 'dd/mm/yyyy'), 222222009);
INSERT INTO Consumo (idConta, dataAberturaConta, nifCamareira, idProduto)VALUES (9, TO_DATE('12/02/2020', 'dd/mm/yyyy'), 333333142, 14); -- Fora dos ultimos 6 meses

INSERT INTO Conta (dataAbertura, nifCliente) VALUES (TO_DATE('12/12/2020', 'dd/mm/yyyy'), 222222010);
INSERT INTO Consumo (idConta, dataAberturaConta, nifCamareira, idProduto)VALUES (10, TO_DATE('12/12/2020', 'dd/mm/yyyy'), 333333142, 15); -- Fora dos ultimos 6 meses

INSERT INTO Conta (dataAbertura, nifCliente) VALUES (TO_DATE('16/09/2020', 'dd/mm/yyyy'), 222222013);
INSERT INTO Consumo (idConta, dataAberturaConta, nifCamareira, idProduto)VALUES (11, TO_DATE('16/09/2020', 'dd/mm/yyyy'), 333333142, 3);

INSERT INTO Conta (dataAbertura, nifCliente) VALUES (TO_DATE('16/09/2020', 'dd/mm/yyyy'), 222222014);
INSERT INTO Consumo (idConta, dataAberturaConta, nifCamareira, idProduto)VALUES (12, TO_DATE('16/09/2020', 'dd/mm/yyyy'), 333333142, 3);

INSERT INTO Conta (dataAbertura, nifCliente) VALUES (TO_DATE('16/09/2020', 'dd/mm/yyyy'), 222222015);
INSERT INTO Consumo (idConta, dataAberturaConta, nifCamareira, idProduto)VALUES (13, TO_DATE('16/09/2020', 'dd/mm/yyyy'), 333333142, 4);

INSERT INTO Conta (dataAbertura, nifCliente) VALUES (TO_DATE('16/09/2020', 'dd/mm/yyyy'), 222222016);
INSERT INTO Consumo (idConta, dataAberturaConta, nifCamareira, idProduto)VALUES (14, TO_DATE('16/09/2020', 'dd/mm/yyyy'), 333333142, 5);
INSERT INTO Consumo (idConta, dataAberturaConta, nifCamareira, idProduto)VALUES (14, TO_DATE('16/09/2020', 'dd/mm/yyyy'), 333333142, 5);
INSERT INTO Consumo (idConta, dataAberturaConta, nifCamareira, idProduto)VALUES (14, TO_DATE('16/09/2020', 'dd/mm/yyyy'), 333333142, 5);
INSERT INTO Consumo (idConta, dataAberturaConta, nifCamareira, idProduto)VALUES (14, TO_DATE('16/09/2020', 'dd/mm/yyyy'), 333333142, 5);
INSERT INTO Consumo (idConta, dataAberturaConta, nifCamareira, idProduto)VALUES (14, TO_DATE('16/09/2020', 'dd/mm/yyyy'), 333333142, 5);
INSERT INTO Consumo (idConta, dataAberturaConta, nifCamareira, idProduto)VALUES (14, TO_DATE('16/09/2020', 'dd/mm/yyyy'), 333333142, 11);
INSERT INTO Consumo (idConta, dataAberturaConta, nifCamareira, idProduto)VALUES (14, TO_DATE('16/09/2020', 'dd/mm/yyyy'), 333333142, 11);
INSERT INTO Consumo (idConta, dataAberturaConta, nifCamareira, idProduto)VALUES (14, TO_DATE('16/09/2020', 'dd/mm/yyyy'), 333333142, 11);


--  FATURAS idFatura INTEGER, idReserva INTEGER, total NUMERIC(7,2), modoPagamento VARCHAR(50), 
--  1 por cada reserva
INSERT INTO Fatura (idReserva, total, modoPagamento) VALUES (1, 100.0, 'Dinheiro');
INSERT INTO Fatura (idReserva, total, modoPagamento) VALUES (2, 100.0, 'Dinheiro');
INSERT INTO Fatura (idReserva, total, modoPagamento) VALUES (3, 100.0, 'Dinheiro');

INSERT INTO Fatura (idReserva, total, modoPagamento) VALUES (4, 100.0, 'Multibanco');
INSERT INTO Fatura (idReserva, total, modoPagamento) VALUES (5, 100.0, 'Multibanco');
INSERT INTO Fatura (idReserva, total, modoPagamento) VALUES (6, 100.0, 'Multibanco');

INSERT INTO Fatura (idReserva, total, modoPagamento) VALUES (7, 100.0, 'MBWAY');
INSERT INTO Fatura (idReserva, total, modoPagamento) VALUES (8, 100.0, 'MBWAY');
INSERT INTO Fatura (idReserva, total, modoPagamento) VALUES (9, 100.0, 'MBWAY');

INSERT INTO Fatura (idReserva, total, modoPagamento) VALUES (10, 100.0, 'Dinheiro');
INSERT INTO Fatura (idReserva, total, modoPagamento) VALUES (11, 100.0, 'Dinheiro');
INSERT INTO Fatura (idReserva, total, modoPagamento) VALUES (12, 100.0, 'Dinheiro');

INSERT INTO Fatura (idReserva, total, modoPagamento) VALUES (13, 100.0, 'Multibanco');
INSERT INTO Fatura (idReserva, total, modoPagamento) VALUES (14, 100.0, 'Multibanco');
INSERT INTO Fatura (idReserva, total, modoPagamento) VALUES (15, 100.0, 'Multibanco');
INSERT INTO Fatura (idReserva, total, modoPagamento) VALUES (16, 100.0, 'Multibanco');


-- =================================================================================================

-- 1.a)
-- Funcionarios
INSERT INTO Funcionario VALUES (444444999, 'Jose', 'jose_manutencao@gmail.com', '+351979797979', 'Casa_6');
INSERT INTO Funcionario VALUES (444444888, 'Ze', 'ze_manutencao@gmail.com', '+351989898989', 'Casa_7');

-- Funcionarios de Manutencao
INSERT INTO Manutencao VALUES (444444999, '+351912222888', null);
INSERT INTO Manutencao VALUES (444444888, '+351912222999', null);

-- Intervencoes

INSERT INTO Intervencao (idQuarto, idAndar, dataRealizacao, estado) VALUES (101, 1, CURRENT_DATE, 'Finalizada'); -- garante que esta data esta dentro das ultimas 48h
INSERT INTO IntervencaoManutencao VALUES (1, 444444999, 'Servico de Manutencao');

INSERT INTO Intervencao (idQuarto, idAndar, dataRealizacao) VALUES (102, 1, TO_DATE('21/03/2020', 'dd/mm/yyyy')); 
INSERT INTO IntervencaoManutencao VALUES (2, 444444999, 'Servico de Manutencao');

INSERT INTO Intervencao (idQuarto, idAndar, dataRealizacao, estado) VALUES (201, 2, TO_DATE('10/01/2020', 'dd/mm/yyyy'), 'Finalizada');
INSERT INTO IntervencaoManutencao VALUES (3, 444444888, 'Servico de Manutencao');

INSERT INTO Intervencao (idQuarto, idAndar, dataRealizacao) VALUES (202, 2, TO_DATE('23/06/2020', 'dd/mm/yyyy')); 
INSERT INTO IntervencaoManutencao VALUES (4, 444444888, 'Servico de Manutencao');

INSERT INTO Intervencao (idQuarto, idAndar, dataRealizacao) VALUES (203, 2,  TO_DATE('11/05/2020', 'dd/mm/yyyy'));
INSERT INTO IntervencaoManutencao VALUES (5, 444444888, 'Servico de Manutencao');

INSERT INTO Intervencao (idQuarto, idAndar, dataRealizacao) VALUES (101, 1,  TO_DATE('27/09/2020', 'dd/mm/yyyy'));
INSERT INTO IntervencaoLimpeza VALUES (6, 333333123); -- intervencao alocada a um funcionario camareira

-- =================================================================================================

-- 1.b)
-- Clientes
INSERT INTO Cliente VALUES (238649040, 'Francisco Ferreira', 'francisco_ferreira10@hotmail.com', '+351932857463', 'Norte', 'Porto');
INSERT INTO Cliente VALUES (213997096, 'David Pereira', 'd_pereira@gmail.com', '+351932463857', 'Centro', 'Lisboa');
INSERT INTO Cliente VALUES (251092445, 'Ricardo Rodrigues', 'rrodrigues@gmail.com', '+351932349972', 'Sul', 'Faro');
INSERT INTO Cliente VALUES (251083567, 'Bernardo Gomes', 'bgomes@gmail.com', '+351934765912', 'Sul', 'Beja');

-- Francisco Ferreira (238649040) - Reservas em abril e junho sem suite
INSERT INTO Reserva (nifCliente, idQuarto, idAndar, dataInicio, dataFim, qtdPessoas, estado, epoca, preco) VALUES (238649040, 106, 1, TO_DATE('16/04/2020', 'dd/mm/yyyy'), TO_DATE('18/04/2020', 'dd/mm/yyyy'), 1, 'Reservada', 'Media', 130.40);
INSERT INTO Reserva (nifCliente, idQuarto, idAndar, dataInicio, dataFim, qtdPessoas, estado, epoca, preco) VALUES (238649040, 206, 2, TO_DATE('03/06/2020', 'dd/mm/yyyy'), TO_DATE('07/06/2020', 'dd/mm/yyyy'), 4, 'Reservada', 'Media', 420.10);

-- David Pereira (213997096) - Reservas em abril e junho com suite
INSERT INTO Reserva (nifCliente, idQuarto, idAndar, dataInicio, dataFim, qtdPessoas, estado, epoca, preco) VALUES (213997096, 106, 1, TO_DATE('20/04/2020', 'dd/mm/yyyy'), TO_DATE('22/04/2020', 'dd/mm/yyyy'), 1, 'Reservada', 'Media', 130.40);
INSERT INTO Reserva (nifCliente, idQuarto, idAndar, dataInicio, dataFim, qtdPessoas, estado, epoca, preco) VALUES (213997096, 206, 2, TO_DATE('08/06/2020', 'dd/mm/yyyy'), TO_DATE('11/06/2020', 'dd/mm/yyyy'), 4, 'Reservada', 'Media', 420.10);
INSERT INTO Reserva (nifCliente, idQuarto, idAndar, dataInicio, dataFim, qtdPessoas, estado, epoca, preco) VALUES (213997096, 101, 1, TO_DATE('03/06/2020', 'dd/mm/yyyy'), TO_DATE('07/06/2020', 'dd/mm/yyyy'), 2, 'Reservada', 'Media', 540.10);

-- Ricardo Rodrigues (251092445) - Reservas em abril e junho sem suite
INSERT INTO Reserva (nifCliente, idQuarto, idAndar, dataInicio, dataFim, qtdPessoas, estado, epoca, preco) VALUES (251092445, 106, 1, TO_DATE('24/04/2020', 'dd/mm/yyyy'), TO_DATE('26/04/2020', 'dd/mm/yyyy'), 1, 'Reservada', 'Media', 130.40);
INSERT INTO Reserva (nifCliente, idQuarto, idAndar, dataInicio, dataFim, qtdPessoas, estado, epoca, preco) VALUES (251092445, 206, 2, TO_DATE('12/06/2020', 'dd/mm/yyyy'), TO_DATE('14/06/2020', 'dd/mm/yyyy'), 4, 'Reservada', 'Media', 329.10);

-- Bernardo Gomes (251083567) - Reservas em abril, maio e junho com suite 
INSERT INTO Reserva (nifCliente, idQuarto, idAndar, dataInicio, dataFim, qtdPessoas, estado, epoca, preco) VALUES (251083567, 106, 1, TO_DATE('27/04/2020', 'dd/mm/yyyy'), TO_DATE('28/04/2020', 'dd/mm/yyyy'), 1, 'Reservada', 'Media', 130.40);
INSERT INTO Reserva (nifCliente, idQuarto, idAndar, dataInicio, dataFim, qtdPessoas, estado, epoca, preco) VALUES (251083567, 101, 1, TO_DATE('22/06/2020', 'dd/mm/yyyy'), TO_DATE('24/06/2020', 'dd/mm/yyyy'), 2, 'Reservada', 'Alta', 540.10);
INSERT INTO Reserva (nifCliente, idQuarto, idAndar, dataInicio, dataFim, qtdPessoas, estado, epoca, preco) VALUES (251083567, 206, 2, TO_DATE('22/07/2020', 'dd/mm/yyyy'), TO_DATE('24/07/2020', 'dd/mm/yyyy'), 4, 'Reservada', 'Media', 720.10);

-- =================================================================================================

--2.a)
-- RESERVAS

--Quartos 104 e 103 reservados pelo Jos� Silva(NIF:222222002) com o estado 'Finalizada'
INSERT INTO Reserva (nifCliente, idQuarto, idAndar, dataInicio, dataFim, qtdPessoas, estado, epoca, preco) VALUES (222222002, 103, 1, TO_DATE('22/01/2020', 'dd/mm/yyyy'), TO_DATE('24/01/2020', 'dd/mm/yyyy'), 2, 'Finalizada', 'Baixa', 100.0); 
INSERT INTO Reserva (nifCliente, idQuarto, idAndar, dataInicio, dataFim, qtdPessoas, estado, epoca, preco) VALUES (222222002, 104, 1, TO_DATE('25/01/2020', 'dd/mm/yyyy'), TO_DATE('27/01/2020', 'dd/mm/yyyy'), 2, 'Finalizada', 'Baixa', 100.0); 

--Reservas nos quartos reservados pelo Jos� Silva
INSERT INTO Reserva (nifCliente, idQuarto, idAndar, dataInicio, dataFim, qtdPessoas, estado, epoca, preco) VALUES (222222014, 102, 1, TO_DATE('18/11/2020', 'dd/mm/yyyy'), TO_DATE('20/11/2020', 'dd/mm/yyyy'), 2, 'Reservada', 'Baixa', 100.0); 
INSERT INTO Reserva (nifCliente, idQuarto, idAndar, dataInicio, dataFim, qtdPessoas, estado, epoca, preco) VALUES (222222003, 103, 1, TO_DATE('18/01/2020', 'dd/mm/yyyy'), TO_DATE('18/01/2020', 'dd/mm/yyyy'), 2, 'Reservada', 'Baixa', 100.0); 
INSERT INTO Reserva (nifCliente, idQuarto, idAndar, dataInicio, dataFim, qtdPessoas, estado, epoca, preco) VALUES (222222006, 103, 1, TO_DATE('20/01/2020', 'dd/mm/yyyy'), TO_DATE('21/01/2020', 'dd/mm/yyyy'), 2, 'Reservada', 'Baixa', 100.0); 
INSERT INTO Reserva (nifCliente, idQuarto, idAndar, dataInicio, dataFim, qtdPessoas, estado, epoca, preco) VALUES (222222007, 104, 1, TO_DATE('20/02/2020', 'dd/mm/yyyy'), TO_DATE('21/02/2020', 'dd/mm/yyyy'), 2, 'Finalizada', 'Baixa', 100.0); 
INSERT INTO Reserva (nifCliente, idQuarto, idAndar, dataInicio, dataFim, qtdPessoas, estado, epoca, preco) VALUES (222222005, 104, 1, TO_DATE('18/02/2020', 'dd/mm/yyyy'), TO_DATE('19/02/2020', 'dd/mm/yyyy'), 2, 'Finalizada', 'Baixa', 100.0); 

-- =================================================================================================

--2.b)

-- (333333123, 333333567, 333333142, 333333652, 333333834);
-- Quartos = (103, 105, 106, 108, 205, 206, 207, 208)
-- Junho - 333333142
INSERT INTO Intervencao (idQuarto, idAndar, dataRealizacao) VALUES (101, 1,  TO_DATE('23/06/2020', 'dd/mm/yyyy')); -- N�o � um dos quartos com estadia superior � m�dia
INSERT INTO IntervencaoLimpeza VALUES (7, 333333123);

INSERT INTO Intervencao (idQuarto, idAndar, dataRealizacao) VALUES (103, 1,  TO_DATE('24/06/2020', 'dd/mm/yyyy'));
INSERT INTO IntervencaoLimpeza VALUES (8, 333333567);

INSERT INTO Intervencao (idQuarto, idAndar, dataRealizacao) VALUES (103, 1,  TO_DATE('26/06/2020', 'dd/mm/yyyy'));
INSERT INTO IntervencaoLimpeza VALUES (9, 333333142);

INSERT INTO Intervencao (idQuarto, idAndar, dataRealizacao) VALUES (103, 1,  TO_DATE('27/06/2020', 'dd/mm/yyyy'));
INSERT INTO IntervencaoLimpeza VALUES (10, 333333142);

-- Julho - 333333123
INSERT INTO Intervencao (idQuarto, idAndar, dataRealizacao) VALUES (101, 1,  TO_DATE('23/07/2020', 'dd/mm/yyyy')); -- N�o � um dos quartos com estadia superior � m�dia
INSERT INTO IntervencaoLimpeza VALUES (11, 333333652);

INSERT INTO Intervencao (idQuarto, idAndar, dataRealizacao) VALUES (103, 1,  TO_DATE('24/07/2020', 'dd/mm/yyyy'));
INSERT INTO IntervencaoLimpeza VALUES (12, 333333142);

INSERT INTO Intervencao (idQuarto, idAndar, dataRealizacao) VALUES (103, 1,  TO_DATE('26/07/2020', 'dd/mm/yyyy'));
INSERT INTO IntervencaoLimpeza VALUES (13, 333333123);

INSERT INTO Intervencao (idQuarto, idAndar, dataRealizacao) VALUES (103, 1,  TO_DATE('27/07/2020', 'dd/mm/yyyy'));
INSERT INTO IntervencaoLimpeza VALUES (14, 333333123);

-- Agosto - 333333567
INSERT INTO Intervencao (idQuarto, idAndar, dataRealizacao) VALUES (101, 1,  TO_DATE('23/08/2020', 'dd/mm/yyyy')); -- N�o � um dos quartos com estadia superior � m�dia
INSERT INTO IntervencaoLimpeza VALUES (15, 333333652);

INSERT INTO Intervencao (idQuarto, idAndar, dataRealizacao) VALUES (103, 1,  TO_DATE('24/08/2020', 'dd/mm/yyyy'));
INSERT INTO IntervencaoLimpeza VALUES (16, 333333142);

INSERT INTO Intervencao (idQuarto, idAndar, dataRealizacao) VALUES (103, 1,  TO_DATE('26/08/2020', 'dd/mm/yyyy'));
INSERT INTO IntervencaoLimpeza VALUES (17, 333333567);

INSERT INTO Intervencao (idQuarto, idAndar, dataRealizacao) VALUES (103, 1,  TO_DATE('27/08/2020', 'dd/mm/yyyy'));
INSERT INTO IntervencaoLimpeza VALUES (18, 333333567);

INSERT INTO Intervencao (idQuarto, idAndar, dataRealizacao) VALUES (103, 1,  TO_DATE('27/08/2020', 'dd/mm/yyyy'));
INSERT INTO IntervencaoLimpeza VALUES (19, 333333567);

-- Setembro - 333333142 e 333333652
INSERT INTO Intervencao (idQuarto, idAndar, dataRealizacao) VALUES (101, 1,  TO_DATE('23/09/2020', 'dd/mm/yyyy')); -- N�o � um dos quartos com estadia superior � m�dia
INSERT INTO IntervencaoLimpeza VALUES (20, 333333567);

INSERT INTO Intervencao (idQuarto, idAndar, dataRealizacao) VALUES (103, 1,  TO_DATE('24/09/2020', 'dd/mm/yyyy'));
INSERT INTO IntervencaoLimpeza VALUES (21, 333333142);

INSERT INTO Intervencao (idQuarto, idAndar, dataRealizacao) VALUES (103, 1,  TO_DATE('26/09/2020', 'dd/mm/yyyy'));
INSERT INTO IntervencaoLimpeza VALUES (22, 333333142);

INSERT INTO Intervencao (idQuarto, idAndar, dataRealizacao) VALUES (103, 1,  TO_DATE('27/09/2020', 'dd/mm/yyyy'));
INSERT INTO IntervencaoLimpeza VALUES (23, 333333652);

INSERT INTO Intervencao (idQuarto, idAndar, dataRealizacao) VALUES (103, 1,  TO_DATE('27/09/2020', 'dd/mm/yyyy'));
INSERT INTO IntervencaoLimpeza VALUES (24, 333333652);

-- Outubro - 333333123
INSERT INTO Intervencao (idQuarto, idAndar, dataRealizacao) VALUES (101, 1,  TO_DATE('23/10/2020', 'dd/mm/yyyy')); -- N�o � um dos quartos com estadia superior � m�dia
INSERT INTO IntervencaoLimpeza VALUES (25, 333333123);

INSERT INTO Intervencao (idQuarto, idAndar, dataRealizacao) VALUES (103, 1,  TO_DATE('24/10/2020', 'dd/mm/yyyy'));
INSERT INTO IntervencaoLimpeza VALUES (26, 333333123);

INSERT INTO Intervencao (idQuarto, idAndar, dataRealizacao) VALUES (103, 1,  TO_DATE('26/10/2020', 'dd/mm/yyyy'));
INSERT INTO IntervencaoLimpeza VALUES (27, 333333123);

INSERT INTO Intervencao (idQuarto, idAndar, dataRealizacao) VALUES (103, 1,  TO_DATE('27/10/2020', 'dd/mm/yyyy'));
INSERT INTO IntervencaoLimpeza VALUES (28, 333333123);

INSERT INTO Intervencao (idQuarto, idAndar, dataRealizacao) VALUES (103, 1,  TO_DATE('27/10/2020', 'dd/mm/yyyy'));
INSERT INTO IntervencaoLimpeza VALUES (29, 333333123);

-- Novembro - 333333834
INSERT INTO Intervencao (idQuarto, idAndar, dataRealizacao) VALUES (101, 1,  TO_DATE('23/11/2020', 'dd/mm/yyyy')); -- N�o � um dos quartos com estadia superior � m�dia
INSERT INTO IntervencaoLimpeza VALUES (30, 333333834);

INSERT INTO Intervencao (idQuarto, idAndar, dataRealizacao) VALUES (103, 1,  TO_DATE('24/11/2020', 'dd/mm/yyyy'));
INSERT INTO IntervencaoLimpeza VALUES (31, 333333834);

INSERT INTO Intervencao (idQuarto, idAndar, dataRealizacao) VALUES (103, 1,  TO_DATE('26/11/2020', 'dd/mm/yyyy'));
INSERT INTO IntervencaoLimpeza VALUES (32, 333333834);

-- Janeiro - N�o faz parte dos ultimos 6 meses
INSERT INTO Intervencao (idQuarto, idAndar, dataRealizacao) VALUES (103, 1,  TO_DATE('27/01/2020', 'dd/mm/yyyy'));
INSERT INTO IntervencaoLimpeza VALUES (33, 333333123);

INSERT INTO Intervencao (idQuarto, idAndar, dataRealizacao) VALUES (103, 1,  TO_DATE('27/01/2020', 'dd/mm/yyyy'));
INSERT INTO IntervencaoLimpeza VALUES (34, 333333123);

-- =================================================================================================

--3.b)

-- Reserva Suite Epoca Alta - 222222016
INSERT INTO Reserva (nifCliente, idQuarto, idAndar, dataInicio, dataFim, qtdPessoas, estado, epoca, preco) VALUES (222222016, 105, 1, TO_DATE('18/08/2020', 'dd/mm/yyyy'), TO_DATE('19/08/2020', 'dd/mm/yyyy'), 2, 'Finalizada', 'Alta', 100.0); 

INSERT INTO Conta (dataAbertura, nifCliente) VALUES (TO_DATE('16/09/2017', 'dd/mm/yyyy'), 251083567);
INSERT INTO Consumo (idConta, dataAberturaConta, nifCamareira, idProduto)VALUES (15, TO_DATE('16/09/2017', 'dd/mm/yyyy'), 333333142, 11);	-- Fora dos ultimos 2 anos
INSERT INTO Consumo (idConta, dataAberturaConta, nifCamareira, idProduto)VALUES (15, TO_DATE('16/09/2017', 'dd/mm/yyyy'), 333333142, 5);	-- Fora dos ultimos 2 anos