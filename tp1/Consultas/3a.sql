WITH aux AS(
    SELECT q.idAndar, q.idQuarto, tq.descricao AS "TIPO_QUARTO", COUNT(*) AS "NR_RESERVAS"
    FROM Quarto q
    INNER JOIN TipoQuarto tq
    ON q.idTipoQuarto = tq.idTipoQuarto
    INNER JOIN Andar a
    ON a.idAndar = q.idAndar
    INNER JOIN Reserva r
    ON r.idAndar = q.idAndar AND r.idQuarto = q.idQuarto
    GROUP BY q.idAndar, q.idQuarto, tq.descricao
    HAVING COUNT(*) >= 2  AND UPPER(tq.descricao) NOT LIKE UPPER('SINGLE')
    ORDER BY 1, 2
)
SELECT idAndar, idQuarto, tipo_quarto, nr_reservas
FROM (
    SELECT aux.*,
        DENSE_RANK() OVER (PARTITION BY idAndar ORDER BY nr_reservas DESC) AS "DRANK"
    FROM aux
) aux1
WHERE drank <= 1
ORDER BY 1;