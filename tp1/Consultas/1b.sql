-- Apresentar a data, a hora e o nome dos clientes que reservaram quartos
-- somente durante o mês de Abril e Junho deste ano. No caso de algum cliente ter
-- reservado um quarto do tipo suite, deverá apresentar a localidade desse cliente
-- numa coluna intitulada “Zona do País”. O resultado deverá ser apresentado por
-- ordem alfabética do nome de cliente e por ordem descendente da data e hora dareserva.

WITH aux AS(
    SELECT r.dataInicio, c.nome, tq.descricao, r.nifCliente, UPPER(TO_CHAR(r.dataInicio, 'MONTH'))
    FROM Reserva r
    INNER JOIN Cliente c
    ON r.nifCliente = c.nif
    INNER JOIN Quarto q
    ON r.idQuarto = q.idQuarto AND r.idAndar = q.idAndar
    INNER JOIN TipoQuarto tq
    ON q.idTipoQuarto = tq.idTipoQuarto
)
SELECT cli.nome, auxTable.dataInicio,
            NVL(
            (
                SELECT cli.localidade
                FROM Reserva r 
                INNER JOIN Quarto q
                ON r.idQuarto = q.idQuarto AND r.idAndar = q.idAndar
                INNER JOIN TipoQuarto tq
                ON q.idTipoQuarto = tq.idTipoQuarto
                WHERE auxTable.nifCliente = r.nifCliente AND auxTable.dataInicio = r.dataInicio AND UPPER(tq.descricao) = UPPER('Suite')
            ), ' ') AS "Zona do País"
FROM
(
    SELECT r.nifCliente, r.dataInicio
    FROM
    (
        ( -- Lista de Clientes que efetuaram reservas em Abril
            SELECT aux.nifCliente
            FROM aux
            WHERE EXTRACT(MONTH FROM aux.dataInicio) IN (4)
        )
        INTERSECT -- A interseção dá-nos o NIF dos Clientes que fizeram reservas nos 2 Meses.
        ( -- Lista de Clientes que efetuaram reservas em Junho
            SELECT aux.nifCliente
            FROM aux
            WHERE EXTRACT(MONTH FROM aux.dataInicio) IN (6)
        )
        MINUS -- Retiramos os Clientes que efetuaram Reservas noutros meses
        (
            SELECT aux.nifCliente
            FROM aux
            WHERE EXTRACT(MONTH FROM aux.dataInicio) NOT IN (4, 6)
        )
    ) auxIntersect
    JOIN Reserva r
    ON (r.nifCliente = auxIntersect.nifCliente)
) auxTable
INNER JOIN Cliente cli
ON cli.nif = auxTable.nifCliente
ORDER BY cli.nome ASC, auxTable.dataInicio DESC;