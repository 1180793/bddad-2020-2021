SELECT *
FROM Intervencao
WHERE idIntervencao IN 
(
    SELECT idIntervencao -- intervencoes alocadas aos empregados que NAO fizeram nenhuma intervencao nos ultimos 2 dias
    FROM IntervencaoManutencao     
    WHERE nifManutencao NOT IN 
    (
        SELECT nifManutencao -- nif dos empregados com intervencoes feitas nos ultimos 2 dias
        FROM IntervencaoManutencao 
        WHERE idIntervencao IN 
        (
            SELECT idIntervencao -- todas as intervencoes feitas nos ultimos 2 dias
            FROM Intervencao i
            WHERE i.dataRealizacao >= CURRENT_DATE - 2
        )
    )
)
AND UPPER(estado) = UPPER('Aberta'); -- intervencoes abertas