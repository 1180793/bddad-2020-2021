-- Apresentar os clientes que ocuparam quartos do tipo suite na última época alta
-- e consumiram os dois produtos com maior consumo nos últimos dois anos. O
-- resultado deve ser ordenado por ordem decrescente do valor total do consumo.

WITH auxProdutos AS
(
    SELECT p.idProduto, p.nome, p.tipoProduto, p.preco, COUNT(*) AS "NR_CONSUMOS"
    FROM Produto p
    INNER JOIN Consumo cons
    ON p.idProduto = cons.idProduto
    WHERE cons.dataAberturaConta >= add_months(sysdate, -24)
    GROUP BY p.idProduto, p.nome, p.tipoProduto, p.preco
),
aux AS(
    SELECT conta.nifCliente, cons.*, p.preco
    FROM Consumo cons
    INNER JOIN Produto p
    ON cons.idProduto = p.idProduto
    INNER JOIN Conta conta
    ON cons.idConta = conta.idConta
    WHERE conta.nifCliente IN 
    (
        SELECT cli.nif
        FROM Cliente cli
        INNER JOIN Reserva r
        ON cli.nif = r.nifCliente
        INNER JOIN Quarto q
        ON r.idQuarto = q.idQuarto AND r.idAndar = q.idAndar
        INNER JOIN TipoQuarto tq
        ON q.idTipoQuarto = tq.idTipoQuarto
        WHERE UPPER(tq.descricao) LIKE UPPER('Suite') AND EXTRACT(YEAR FROM(r.dataInicio)) = 2020 AND UPPER(r.epoca) LIKE UPPER('Alta')
    ) AND cons.idProduto IN
    (
        SELECT idProduto
        FROM (
            SELECT auxProdutos.*,
                DENSE_RANK() OVER (ORDER BY nr_consumos DESC) AS "DRANK"
            FROM auxProdutos
        ) aux1
        WHERE drank <= 2
    )
    ORDER BY 2
)
SELECT auxIntersect.nifCliente, cliente.*
FROM
    (
        ( -- Lista de Clientes que Compraram o Produto mais Comprado
            SELECT aux.nifCliente
            FROM aux
            WHERE aux.idProduto = (
                SELECT idProduto
                FROM (
                    SELECT auxProdutos.*,
                    DENSE_RANK() OVER (ORDER BY nr_consumos DESC) AS "DRANK"
                    FROM auxProdutos
                ) aux1
                WHERE drank <= 1
            )
        )
        INTERSECT
        ( -- Lista de Clientes que Compraram o Segundo Produto mais Comprado
            SELECT aux.nifCliente
            FROM aux
            WHERE aux.idProduto = (
                SELECT idProduto
                FROM (
                    SELECT auxProdutos.*,
                    DENSE_RANK() OVER (ORDER BY nr_consumos DESC) AS "DRANK"
                    FROM auxProdutos
                ) aux1
                WHERE drank = 2
            )
        )
    ) auxIntersect
INNER JOIN Cliente cliente
ON (cliente.nif = auxIntersect.nifCliente)
INNER JOIN Conta conta
ON (conta.nifCliente = auxIntersect.nifCliente)
ORDER BY (
    SELECT SUM(p.preco)
    FROM Conta c
    INNER JOIN Consumo cons
    ON c.idConta = cons.idConta
    INNER JOIN Produto p
    ON cons.idProduto = p.idProduto
    WHERE c.nifCliente = auxIntersect.nifCliente
) DESC;

