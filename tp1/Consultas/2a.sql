SELECT DISTINCT c.nome, c.localidade, c.concelho
FROM Cliente c 
INNER JOIN Reserva r
ON c.nif = r.nifCliente
INNER JOIN 
(
    SELECT r1.idQuarto, r1.idAndar
    FROM Reserva r1 
    INNER JOIN Cliente c1
    ON c1.nif = r1.nifCliente
    WHERE UPPER(c1.nome) = UPPER('Jose Silva') AND UPPER(c1.concelho) = UPPER('Vila Real') AND r1.estado = 'Finalizada'
) aux1
ON (r.idQuarto = aux1.idQuarto AND r.idAndar = aux1.idAndar)
MINUS
SELECT c.nome, c.localidade, c.concelho
FROM Cliente c 
INNER JOIN Reserva r
ON c.nif = r.nifCliente
WHERE UPPER(c.nome) = UPPER('Jose Silva') AND UPPER(c.concelho) = UPPER('Vila Real');