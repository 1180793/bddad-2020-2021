WITH aux AS(
    SELECT il.nifCamareira, f.nome, UPPER(TO_CHAR(i.dataRealizacao, 'MONTH')) AS "MES", COUNT(*) AS "QTD_INTERVENCOES"
    FROM IntervencaoLimpeza il
    INNER JOIN Intervencao i
    ON il.idIntervencao = i.idIntervencao
    INNER JOIN Funcionario f
    ON il.nifCamareira = f.nif
    INNER JOIN 
    (
        SELECT idQuarto, idAndar
        FROM Reserva r
        WHERE r.dataFim - r.dataInicio > (SELECT AVG(dataFim - dataInicio) FROM Reserva)
    ) aux1
    ON (i.idQuarto = aux1.idQuarto AND i.idAndar = aux1.idAndar)
    WHERE i.dataRealizacao >= add_months(sysdate, -6)
    GROUP BY il.nifCamareira, f.nome, UPPER(TO_CHAR(i.dataRealizacao, 'MONTH'))
)
SELECT mes, nifCamareira, nome, qtd_intervencoes
FROM (
    SELECT aux.*,
        DENSE_RANK() OVER (PARTITION BY mes ORDER BY qtd_intervencoes DESC) AS "DRANK"
    FROM aux
)
WHERE drank <= 1
ORDER BY TO_CHAR(TO_DATE(mes,'MONTH'), 'mm');