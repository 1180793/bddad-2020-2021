------------------------------------------------------------------------------------------------------
-- Cria��o da Base de Dados - SweetDreams
------------------------------------------------------------------------------------------------------

DROP TABLE Cliente 					CASCADE CONSTRAINTS PURGE;
DROP TABLE Andar 					CASCADE CONSTRAINTS PURGE;
DROP TABLE TipoQuarto 				CASCADE CONSTRAINTS PURGE;
DROP TABLE Quarto 					CASCADE CONSTRAINTS PURGE;
DROP TABLE Reserva 					CASCADE CONSTRAINTS PURGE;
DROP TABLE Funcionario 				CASCADE CONSTRAINTS PURGE;
DROP TABLE Camareira 				CASCADE CONSTRAINTS PURGE;
DROP TABLE Rececao 					CASCADE CONSTRAINTS PURGE;
DROP TABLE Restaurante 				CASCADE CONSTRAINTS PURGE;
DROP TABLE Manutencao 				CASCADE CONSTRAINTS PURGE;
DROP TABLE Conta 					CASCADE CONSTRAINTS PURGE;
DROP TABLE Produto					CASCADE CONSTRAINTS PURGE;
DROP TABLE Consumo 					CASCADE CONSTRAINTS PURGE;
DROP TABLE Intervencao 				CASCADE CONSTRAINTS PURGE;
DROP TABLE IntervencaoLimpeza    	CASCADE CONSTRAINTS PURGE;
DROP TABLE IntervencaoManutencao 	CASCADE CONSTRAINTS PURGE;
DROP TABLE Fatura 					CASCADE CONSTRAINTS PURGE;

-- Cliente
CREATE TABLE Cliente (
	nif 		INTEGER 		CONSTRAINT pkClienteNif			PRIMARY KEY
								CONSTRAINT ckClienteNif   		CHECK(REGEXP_LIKE(nif, '^\d{9}$')),
	nome 		VARCHAR(50) 	CONSTRAINT nnClienteNome    	NOT NULL,
    email 		VARCHAR(50)		CONSTRAINT ukClienteEmail		UNIQUE,
    telefone 	VARCHAR(15),
    localidade 	VARCHAR(15)		CONSTRAINT nnClienteLocalidade 	NOT NULL,
    concelho 	VARCHAR(50)		CONSTRAINT nnClienteConcelho 	NOT NULL
);
ALTER TABLE Cliente	ADD  CONSTRAINT ckClientNullEmailOuTelefone	CHECK (email IS NOT NULL OR telefone IS NOT NULL);

-- Andar
CREATE TABLE Andar (
    idAndar 	INTEGER			CONSTRAINT pkAndarIDAndar		PRIMARY KEY,
    nome 		VARCHAR(20)		CONSTRAINT nnAndarNome			NOT NULL
);

-- TipoQuarto
CREATE TABLE TipoQuarto (
    idTipoQuarto	INTEGER		CONSTRAINT pkTipoQuartoIDTipoQuarto PRIMARY KEY,
    descricao		VARCHAR(20)	CONSTRAINT nnTipoQuartoDescricao	NOT NULL
);

-- Quarto
CREATE TABLE Quarto (
    idQuarto 		INTEGER,
    idAndar 		INTEGER,   
	idTipoQuarto 	INTEGER	CONSTRAINT nnQuartoIdTipoQuarto	NOT NULL,
    lotacao 		INTEGER	CONSTRAINT nnQuartoLotacao		NOT NULL,
	CONSTRAINT pkQuartoIdQuartoIdAndar PRIMARY KEY(idQuarto, idAndar)
);
ALTER TABLE Quarto ADD CONSTRAINT fkQuartoIdAndar 		FOREIGN KEY(idAndar) 		REFERENCES Andar(idAndar);
ALTER TABLE Quarto ADD CONSTRAINT fkQuartoIdTipoQuarto 	FOREIGN KEY(idTipoQuarto) 	REFERENCES TipoQuarto(idTipoQuarto);

-- Reserva
CREATE TABLE Reserva (
    idReserva	INTEGER GENERATED AS IDENTITY	CONSTRAINT pkReservaIdReserva 	PRIMARY KEY,
	nifCliente 	INTEGER							CONSTRAINT nnReservaNifCliente	NOT NULL,
    idQuarto	INTEGER							CONSTRAINT nnReservaIdQuarto	NOT NULL,
    idAndar 	INTEGER							CONSTRAINT nnReservaIdAndar		NOT NULL,
    dataInicio 	DATE							CONSTRAINT nnReservaDataInicio	NOT NULL,
    dataFim 	DATE							CONSTRAINT nnReservaDataFim		NOT NULL,
    qtdPessoas 	INTEGER							CONSTRAINT nnReservaQtdPessoas	NOT NULL,   
    estado VARCHAR(15)	DEFAULT 'Reservada',	
    epoca VARCHAR(10)							CONSTRAINT nnReservaEpoca		NOT NULL,   
    preco NUMERIC(7,2)							CONSTRAINT nnReservaPreco		NOT NULL
);
ALTER TABLE Reserva ADD CONSTRAINT fkReservaNifCliente 	FOREIGN KEY(nifCliente) 			REFERENCES Cliente(nif);
ALTER TABLE Reserva ADD CONSTRAINT fkReservaQuarto 		FOREIGN KEY(idQuarto, idAndar) 	REFERENCES Quarto(idQuarto, idAndar);

-- Funcionario
CREATE TABLE Funcionario (    
    nif 		INTEGER 		CONSTRAINT pkFuncionarioNif			PRIMARY KEY
								CONSTRAINT ckFuncionarioNif   		CHECK(REGEXP_LIKE(nif, '^\d{9}$')),
	nome 		VARCHAR(50) 	CONSTRAINT nnFuncionarioNome    	NOT NULL,
    email 		VARCHAR(50)		CONSTRAINT nnFuncionarioEmail		NOT NULL
								CONSTRAINT ukFuncionarioEmail		UNIQUE,
    telefone 	VARCHAR(15)		CONSTRAINT nnFuncionarioTelefone	NOT NULL,
    morada 		VARCHAR(15)		CONSTRAINT nnFuncionarioMorada 		NOT NULL
);

-- Funcionario - Camareira
CREATE TABLE Camareira (    
    nif 	INTEGER		CONSTRAINT pkCamareiraNif	PRIMARY KEY
);
ALTER TABLE Camareira ADD CONSTRAINT fkCamareiraNifFuncionario FOREIGN KEY(nif) REFERENCES Funcionario(nif);

-- Funcionario - Rececao
CREATE TABLE Rececao (    
    nif 	INTEGER		CONSTRAINT pkRececaoNif	PRIMARY KEY
);
ALTER TABLE Rececao ADD CONSTRAINT fkRececaoNifFuncionario FOREIGN KEY(nif) REFERENCES Funcionario(nif);

-- Funcionario - Restaurante
CREATE TABLE Restaurante (    
    nif 	INTEGER		CONSTRAINT pkRestauranteNif	PRIMARY KEY
);
ALTER TABLE Restaurante ADD CONSTRAINT fkRestauranteNifFuncionario FOREIGN KEY(nif) REFERENCES Funcionario(nif);

-- Funcionario - Manutencao
CREATE TABLE Manutencao (    
    nif 			INTEGER		CONSTRAINT pkManutencaoNif				PRIMARY KEY,
    telefoneServico VARCHAR(15)	CONSTRAINT nnManutencaoTelefoneServico	NOT NULL,
    nifResponsavel 	INTEGER
);
ALTER TABLE Manutencao ADD CONSTRAINT fkManutencaoNifFuncionario 	FOREIGN KEY(nif) 			REFERENCES Funcionario(nif);
ALTER TABLE Manutencao ADD CONSTRAINT fkManutencaoResponsavel 		FOREIGN KEY(nifResponsavel) REFERENCES Manutencao(nif);

-- Conta
CREATE TABLE Conta (    
    idConta			INTEGER GENERATED AS IDENTITY,
    dataAbertura 	DATE,
    nifCliente 		INTEGER 						CONSTRAINT nnContaNifCliente	NOT NULL,
    despesas 		NUMERIC(7,2) DEFAULT 0.00,
	CONSTRAINT pkContaIdContaDataAbertura PRIMARY KEY(idConta, dataAbertura)
);
ALTER TABLE Conta ADD CONSTRAINT fkContaNifCliente FOREIGN KEY(nifCliente) REFERENCES Cliente(nif);

-- Produto
CREATE TABLE Produto(
	idProduto	INTEGER 		CONSTRAINT pkProdutoIdProduto	PRIMARY KEY,
	nome 		VARCHAR(25)		CONSTRAINT nnProdutoNome		NOT NULL,
	tipoProduto	VARCHAR(15) 	CONSTRAINT nnProdutoTipoProduto	NOT NULL,
	preco		NUMERIC(7,2)	CONSTRAINT nnProdutoPreco		NOT NULL
);

-- Consumo
CREATE TABLE Consumo (
	idConsumo			INTEGER GENERATED AS IDENTITY 	CONSTRAINT pkConsumoIdConsumo 			PRIMARY KEY, 
	idConta				INTEGER							CONSTRAINT nnConsumoIdConta				NOT NULL,
	dataAberturaConta	DATE							CONSTRAINT nnConsumoDataAberturaConta	NOT NULL,
	nifCamareira		INTEGER							CONSTRAINT nnConsumoNifCamareira		NOT NULL,
	idProduto			INTEGER							CONSTRAINT nnConsumoProduto				NOT NULL
);
ALTER TABLE Consumo	ADD CONSTRAINT fkConsumoConta 		FOREIGN KEY(idConta, dataAberturaConta) REFERENCES Conta(idConta, dataAbertura);
ALTER TABLE Consumo ADD CONSTRAINT fkConsumoCamareira	FOREIGN KEY(nifCamareira)				REFERENCES Camareira(nif);
ALTER TABLE Consumo ADD CONSTRAINT fkConsumoProduto		FOREIGN KEY(idProduto)					REFERENCES Produto(idProduto);

-- Intervencao
CREATE TABLE Intervencao (   
    idIntervencao 		INTEGER GENERATED AS IDENTITY 	CONSTRAINT pkIntervencaoIdIntervencao 	PRIMARY KEY, 
    idQuarto 			INTEGER							CONSTRAINT nnIntervencaoIdQuarto		NOT NULL,
    idAndar 			INTEGER							CONSTRAINT nnIntervencaoIdAndar			NOT NULL,
    dataRealizacao 		DATE							CONSTRAINT nnIntervencaoDataRealizacao	NOT NULL,
	estado				VARCHAR(15) DEFAULT 'ABERTA'	CONSTRAINT nnIntervencaoEstado			NOT NULL
);
ALTER TABLE Intervencao ADD CONSTRAINT fkIntervencaoQuarto 			FOREIGN KEY(idQuarto, idAndar) 	REFERENCES Quarto(idQuarto, idAndar);

-- Intervencao Limpeza
CREATE TABLE IntervencaoLimpeza (    
    idIntervencao 		INTEGER		CONSTRAINT pkIntervencaoLimpezaIdIntervencao 	PRIMARY KEY,
	nifCamareira		INTEGER		CONSTRAINT nnIntervencaoLimpezaNifCamareira		NOT NULL
);
ALTER TABLE IntervencaoLimpeza ADD CONSTRAINT fkIntervencaoLimpezaIdIntervencao FOREIGN KEY(idIntervencao) 	REFERENCES Intervencao(idIntervencao);
ALTER TABLE IntervencaoLimpeza ADD CONSTRAINT fkIntervencaoLimpezaNifCamareira 	FOREIGN KEY(nifCamareira)	REFERENCES Camareira(nif);

-- Intervencao Manutencao
CREATE TABLE IntervencaoManutencao (    
    idIntervencao 		INTEGER		CONSTRAINT pkIntervencaoManutencaoIdIntervencao 	PRIMARY KEY,
	nifManutencao		INTEGER		CONSTRAINT nnIntervencaoManutencaoNifManutencao		NOT NULL,
	descricao			VARCHAR(50)	CONSTRAINT nnIntervencaoManutencaoDescricao			NOT NULL
);
ALTER TABLE IntervencaoManutencao ADD CONSTRAINT fkIntervencaoManutencaoIdIntervencao 	FOREIGN KEY(idIntervencao) 	REFERENCES Intervencao(idIntervencao);
ALTER TABLE IntervencaoManutencao ADD CONSTRAINT fkIntervencaoManutencaoNifManutencao 	FOREIGN KEY(nifManutencao)	REFERENCES Manutencao(nif);

-- Fatura
CREATE TABLE Fatura (
	idFatura 		INTEGER GENERATED AS IDENTITY	CONSTRAINT pkFaturaIdFatura			PRIMARY KEY, 
	idReserva		INTEGER							CONSTRAINT nnFaturaIdReserva		NOT NULL
													CONSTRAINT ukFaturaIdReserva		UNIQUE,
    total 			NUMERIC(7,2)					CONSTRAINT nnFaturaTotal			NOT NULL, 
    modoPagamento 	VARCHAR(255)					CONSTRAINT nnFaturaModoPagamento	NOT NULL
);
ALTER TABLE Fatura ADD CONSTRAINT fkFaturaIdReserva FOREIGN KEY(idReserva) REFERENCES Reserva(idReserva);