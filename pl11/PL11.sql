-- PL11
-- 1. Mostrar o id, o nome e a quantidade de reservas de barcos dos marinheiros registados na BD, por ordem decrescente da 
--  quantidade de reservas (Figura 2). Resolver usando duas estratégias diferentes, nomeadamente:
-- a) Com junção de tabelas e sem subqueries;
SELECT a.idMarinheiro, a.nome, NVL(b.cnt, 0) AS "QTD_RESERVAS"
FROM MARINHEIRO a
LEFT JOIN(
    SELECT r.idMarinheiro, COUNT(*) AS "CNT"
    FROM RESERVA r
    GROUP BY idMarinheiro) b
ON (a.idMarinheiro = b.idMarinheiro)
ORDER BY 3 DESC;

-- b) Sem junção de tabelas.
SELECT a.idMarinheiro, a.nome, (
    SELECT COUNT(*)
    FROM RESERVA r
    WHERE r.idMarinheiro = a.idMarinheiro) AS "QTD_RESERVAS"
FROM MARINHEIRO a
ORDER BY 3 DESC;

-- 2. Mostrar o id dos marinheiros cuja quantidade de reservas de um barco seja superior à quantidade média de reservas desse
--   barco (Figura 3). Além disso, o resultado deve também incluir o id do barco e a quantidade de reservas.
WITH w1 AS (
    SELECT r.idMarinheiro, r.idBarco, COUNT(*) AS "QTD_RESERVAS"
    FROM RESERVA r
    GROUP BY r.idMarinheiro, r.idBarco)
SELECT *
FROM w1 a
WHERE QTD_RESERVAS > (
    SELECT AVG(QTD_RESERVAS)
    FROM w1 b
    WHERE idBarco = a.idBarco)
ORDER BY 3;

-- 3. Copiar e alterar o comando do ponto 1b) de modo a mostrar o id e o nome dos marinheiros cuja quantidade de reservas está 
--   no top 3 do ranking dessa quantidade. O resultado apresentado deve estar de acordo com a Figura 4.
-- OBS: Utilizando Windowed Columns.
WITH w1 AS (
    SELECT a.idMarinheiro, a.nome, (
        SELECT COUNT(*)
        FROM RESERVA r
        WHERE r.idMarinheiro = a.idMarinheiro
        ) AS "QTD_RESERVAS"
    FROM MARINHEIRO a
)
SELECT idMarinheiro, nome, qtd_reservas
FROM (
    SELECT w1.*,
        ROW_NUMBER() OVER (ORDER BY qtd_reservas DESC) AS "ROW_NUMBER",
        RANK() OVER (ORDER BY qtd_reservas DESC) AS "RANK",
        DENSE_RANK() OVER (ORDER BY qtd_reservas DESC) AS "DRANK"
    FROM w1)
WHERE drank <= 3
ORDER BY 3 DESC;

-- 4. Mostrar o nome dos marinheiros que reservaram todos os barcos com a designação Interlake (Figura 5), usando três 
--   estratégias diferentes, nomeadamente:
-- a) Inclusão de conjuntos (i.e. diferença de conjuntos igual a vazio); 
-- Marinheiros
SELECT m.nome
FROM MARINHEIRO m;

-- Barcos
SELECT *
FROM BARCO
WHERE designacao = 'Interlake';

-- Reservas
SELECT *
FROM RESERVA;

--X (A, B)
--Y (A)

-- X - Y = (A,B) - (A) = (B)

SELECT m.nome
FROM MARINHEIRO m
WHERE NOT EXISTS(
    SELECT b.idBarco
    FROM BARCO b
    WHERE b.designacao = 'Interlake'
    MINUS
    SELECT r.idBarco
    FROM RESERVA r
    WHERE r.idMarinheiro = m.idMarinheiro
);

-- b) Comparação de cardinalidades (i.e. quantidades);
WITH W1 AS (
    SELECT b.idBarco
    FROM BARCO b
    WHERE b.designacao = 'Interlake'
)
SELECT m.nome
FROM MARINHEIRO m, 
    (SELECT COUNT(*) QTD FROM W1) y, 
    (SELECT r.idMarinheiro, COUNT(DISTINCT r.idBarco) QTD FROM RESERVA r
    JOIN W1 b ON (r.idBarco = b.idBarco)
    GROUP BY r.idMarinheiro) z
WHERE m.idMarinheiro = z.idMarinheiro and y.qtd = z.qtd;

-- c) Quantificação (i.e. com operador EXISTS).
SELECT m.nome
FROM  MARINHEIRO m
WHERE NOT EXISTS
    (SELECT b.idBarco
    FROM BARCO b
    WHERE b.designacao = 'Interlake' AND 
    NOT EXISTS
        (SELECT r.idBarco
        FROM RESERVA r
        WHERE r.idMarinheiro = m.idmarinheiro and r.idbarco = b.idBarco
));