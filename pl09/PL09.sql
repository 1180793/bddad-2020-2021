-- PL09
-- 1. Mostrar a quantidade de CD comprados em cada um dos locais de compra registados, de acordo com a Figura 2.
SELECT NVL(cd.localCompra, 'Desconhecido' ) AS "LOCAL_COMPRA"  , COUNT(*) AS "QUANTIDADE_CD"
FROM CD cd
GROUP BY cd.localCompra;

-- 2.Copiar e alterar o comando da alínea anterior, de forma a mostrar o resultado por ordem decrescente da quantidade de
--  CD comprados.
SELECT NVL(cd.localCompra, 'Desconhecido' ) AS "LOCAL_COMPRA" , COUNT(*) AS "QUANTIDADE_CD"
FROM CD cd
GROUP BY cd.localCompra
ORDER BY quantidade_cd DESC;

-- 3. Mostrar a quantidade de editoras diferentes que produziram os CD comprados, em cada um dos locais de compra. 
--   O resultado apresentado deve estar de acordo com a Figura 4.
SELECT NVL(cd.localCompra, 'Desconhecido' ), COUNT(DISTINCT cd.idEditora) AS "QUANTIDADE_EDITORAS"
FROM CD cd
GROUP BY cd.localCompra
ORDER BY quantidade_editoras ASC;

-- 4. Copiar e alterar o comando da alínea 2, de forma a mostrar também, para cada local de compra conhecido, o valor total 
--   pago e o maior valor pago. O resultado apresentado deve estar de acordo com a Figura 5.
SELECT cd.localCompra AS "LOCAL_COMPRA" , COUNT(*) AS "QUANTIDADE_CD", SUM(cd.valorPago) AS "TOTAL", MAX(cd.valorPago) AS "MAIOR"
FROM CD cd
WHERE cd.localCompra IS NOT NULL
GROUP BY cd.localCompra
ORDER BY quantidade_cd DESC;

-- 5. Mostrar, para cada CD e respetivos intérpretes, a quantidade de músicas do CD em que o intérprete participa. Além da 
--  quantidade referida, também deve ser apresentado o código do CD e o intérprete, de acordo com a Figura 6.
SELECT m.codCd, m.interprete, COUNT(m.nrMusica) AS "QUANTIDADE_MUSICAS"
FROM MUSICA m
GROUP BY m.codCd, m.interprete
ORDER BY m.codCd;

-- 6. Copiar o comando da alínea 1 e alterar de modo a mostrar apenas os locais de compra com a quantidade superior a 2
SELECT NVL(cd.localCompra, 'Desconhecido' ) AS "LOCAL_COMPRA"  , COUNT(*) AS "QUANTIDADE_CD"
FROM CD cd
GROUP BY cd.localCompra
HAVING COUNT(*) > 2;

-- 7. Mostrar os locais de compra, cuja média do valor pago por CD é inferior a 10, juntamente com o respetivo total do 
--   valor pago.
SELECT cd.localCompra, AVG(cd.valorPago) AS "TOTAL"
FROM CD cd
GROUP BY cd.localCompra
HAVING  AVG(cd.valorPago) < 10;

-- 8. Mostrar o valor total pago nos locais de compra, cuja quantidade de CD comprados é superior a 2. O local de compra 
--   também deve ser visualizado.
SELECT cd.localCompra, SUM(cd.valorPago)
FROM CD cd
GROUP BY cd.localCompra
HAVING COUNT(*) > 2;

-- 9. Copiar o comando da alínea 5 e alterar de modo a mostrar apenas o intérprete e o código do CD em que o intérprete 
--   participa apenas em 1 música. O resultado deve ser apresentado por ordem crescente do código do CD e, em caso de 
--   igualdade, por ordem alfabética do intérprete.
SELECT m.codCd, m.interprete
FROM MUSICA m
GROUP BY m.codCd, m.interprete
HAVING COUNT(m.nrMusica) = 1
ORDER BY m.codCd ASC, m.interprete ASC;

-- 10. Copiar o comando da alínea anterior e alterar de modo a mostrar apenas os intérpretes começados por E ou L
--   (letras maiúsculas ou minúsculas).
SELECT DISTINCT m.interprete
FROM MUSICA m
GROUP BY m.codCd, m.interprete
HAVING COUNT(m.nrMusica) = 1 AND REGEXP_LIKE(m.interprete, '^[el]', 'i')
ORDER BY m.interprete ASC;

-- 11. Mostrar os dias de semana e a respetiva quantidade de CD comprados em locais conhecidos, de acordo com a Figura 12.
SELECT TO_CHAR(cd.dataCompra, 'DAY') AS DIA_SEMANA, COUNT(*) AS "QTD_CD_COMPRADOS"
FROM CD cd
WHERE cd.localCompra IS NOT NULL
GROUP BY TO_CHAR(cd.dataCompra, 'DAY')
HAVING COUNT(*) > 1
ORDER BY QTD_CD_COMPRADOS ASC;

-- 12. Mostrar, para cada CD, o título e a quantidade de músicas
SELECT cd.titulo, COUNT(*) AS "QUANTIDADE_MUSICAS"
FROM MUSICA m, CD cd
WHERE m.codCd = cd.codCd
GROUP BY cd.codCd, cd.titulo
ORDER BY QUANTIDADE_MUSICAS DESC;

-- 13. Mostrar, para cada CD, o código, o título e a quantidade de músicas
SELECT cd.codCd, cd.titulo, COUNT(*) AS "QUANTIDADE_MUSICAS"
FROM MUSICA m, CD cd
WHERE m.codCd = cd.codCd
GROUP BY cd.codCd, cd.titulo
ORDER BY QUANTIDADE_MUSICAS DESC;

-- 14. Mostrar, para cada CD, o código, o título e a quantidade de músicas cuja duração é superior a 5
SELECT cd.codCd, cd.titulo, COUNT(*) AS "QUANTIDADE_MUSICAS"
FROM MUSICA m, CD cd
WHERE m.codCd = cd.codCd AND m.duracao > 5
GROUP BY cd.codCd, cd.titulo
ORDER BY QUANTIDADE_MUSICAS DESC;

-- 15. Mostrar, para cada CD com menos de 6 músicas, o código, o título e a quantidade de músicas do CD
SELECT cd.codCd, cd.titulo, COUNT(*) AS "QUANTIDADE_MUSICAS"
FROM MUSICA m, CD cd
WHERE m.codCd = cd.codCd
GROUP BY cd.codCd, cd.titulo
HAVING COUNT(*) < 6
ORDER BY QUANTIDADE_MUSICAS DESC;

-- 16. Mostrar, para cada CD cujas músicas têm uma duração média superior a 4, o código, o título e a quantidade de 
--    músicas do CD.
SELECT cd.codCd, cd.titulo, COUNT(*) AS "QUANTIDADE_MUSICAS"
FROM MUSICA m, CD cd
WHERE m.codCd = cd.codCd
GROUP BY cd.codCd, cd.titulo
HAVING AVG(m.duracao) > 4
ORDER BY QUANTIDADE_MUSICAS DESC;

-- 17. Mostrar, numa coluna, o título de cada uma das músicas e o título de cada CD que tem pelo menos 3 interpretes. 
--   O resultado apresentado deve estar de acordo com a Figura 18.
SELECT cd.titulo
FROM CD cd
INNER JOIN MUSICA m 
ON m.codCD = cd.codCD 
GROUP BY cd.titulo
HAVING COUNT(DISTINCT m.interprete) >= 3
UNION ALL
SELECT m.titulo
FROM MUSICA m
ORDER BY 1;

-- 18. Copiar e alterar o comando da alínea anterior, de modo a não mostrar os registos repetidos.
SELECT cd.titulo
FROM CD cd 
INNER JOIN MUSICA m 
ON m.codCD = cd.codCD 
GROUP BY cd.titulo
HAVING COUNT(DISTINCT m.interprete) >= 3
UNION
SELECT m.titulo
FROM MUSICA m;

-- 19. Copiar e alterar o comando da alínea anterior, de modo a apresentar também o comprimento de cada título e por ordem
--   decrescente.
SELECT c.titulo, LENGTH(c.titulo) AS "COMPRIMENTO"
FROM CD c 
INNER JOIN MUSICA m 
ON m.codCD = c.codCD 
GROUP BY c.titulo
HAVING COUNT(DISTINCT m.interprete) >= 3
UNION
SELECT m.titulo, LENGTH(m.titulo)
FROM MUSICA m
ORDER BY comprimento DESC;

-- 20. Mostrar os títulos de músicas que são iguais a títulos de CD cuja maior duração de uma música é superior a 5. A solução
--   não deve recorrer ao uso de subqueries
SELECT cd.titulo
FROM CD cd 
INNER JOIN MUSICA m 
ON m.codCD = cd.codCD 
GROUP BY cd.titulo
HAVING MAX(m.duracao) >= 5
INTERSECT
SELECT m.titulo
FROM MUSICA m
INNER JOIN CD cd
ON m.codCD = cd.codCD 
WHERE m.titulo = cd.titulo;
