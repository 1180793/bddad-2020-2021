set serveroutput on;

delete from camareira_bonus;

-- Teste - Superior a 50%
insert into camareira_bonus(id_camareira, bonus) values (14, 51);

-- Teste - Inferior a 0%
insert into camareira_bonus(id_camareira, bonus) values (14, 10);
update camareira_bonus set bonus = -1 where id_camareira = 14;

-- Teste - Bónus igual ao registado atualmente
update camareira_bonus set bonus = 10 where id_camareira = 14;

-- Teste - Bónus menor que o registado atualmente
update camareira_bonus set bonus = 5 where id_camareira = 14;

delete from camareira_bonus;