create or replace trigger trgCorrigirAlteracaoBonus before insert or update on camareira_bonus for each row
declare
	v_bonus number;
begin
	if :new.bonus > 50 then
		raise_application_error(-20026, 'O bónus não pode ser superior a 50%');
	elsif :new.bonus < 0 then
		raise_application_error(-20027, 'O bónus não pode ser inferior a 0%');
	end if;

	if :new.bonus = :old.bonus then
		raise_application_error(-20026, 'O bónus inserido é igual ao bónus registado atualmente');
	end if;

	if :new.bonus < :old.bonus then
		raise_application_error(-20026, 'O bónus inserido não pode ser menor que o bónus registado atualmente');
	end if;
end;
/

-- TESTES

set serveroutput on;

delete from camareira_bonus;

-- Teste - Superior a 50%
insert into camareira_bonus(id_camareira, bonus) values (14, 51);

-- Teste - Inferior a 0%
insert into camareira_bonus(id_camareira, bonus) values (14, 10);
update camareira_bonus set bonus = -1 where id_camareira = 14;

-- Teste - Bónus igual ao registado atualmente
update camareira_bonus set bonus = 10 where id_camareira = 14;

-- Teste - Bónus menor que o registado atualmente
update camareira_bonus set bonus = 5 where id_camareira = 14;

delete from camareira_bonus;