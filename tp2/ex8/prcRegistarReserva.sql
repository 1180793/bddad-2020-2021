create or replace procedure prcRegistarReserva(p_tipo_quarto tipo_quarto.id%type, p_data_entrada date, p_data_saida date, p_nr_pessoas reserva.nr_pessoas%type,
                                p_id_cliente reserva.id_cliente%type default null, p_nome reserva.nome%type default null, p_nif reserva.nif%type default null, 
                                p_telefone reserva.telefone%type default null, p_email reserva.email%type default null)
is
    aux number;
    v_preco number;
    v_id_reserva number;

    ex_tipo_quarto_invalido exception;
    ex_data_invalida exception;
    ex_data_rage_invalida exception;
    ex_nr_pessoas_invalido exception;
    ex_parametro_id exception;
    ex_parametro_nome exception;
    ex_cliente_invalido exception;
begin    
    -- Validação Tipo Quarto
    select count(*) into aux
    from tipo_quarto
    where id = p_tipo_quarto;
    if aux = 0 then
        raise ex_tipo_quarto_invalido;
    end if;

    if p_data_entrada < sysdate then
        raise ex_data_invalida;
    end if;

    -- Validação Datas
    if p_data_entrada >= p_data_saida then
        raise ex_data_rage_invalida;
    end if;

    -- Validação Número de Pessoas
    if p_nr_pessoas <= 0 then
        raise ex_nr_pessoas_invalido;
    end if;

    -- Quando se especifica o parâmetro id do cliente, não se pode especificar o nome, o NIF, o telefone nem o email.
    if (p_id_cliente is not null) then
        if not (p_nome is null and p_nif is null and p_telefone is null and p_email is null) then
            raise ex_parametro_id;
        end if;

        -- Validação ID Cliente
        select count(*) into aux
        from cliente
        where id = p_id_cliente;
        if aux = 0 then
            raise ex_cliente_invalido;
        end if;
    --  Quando não se especifica o parâmetro id do cliente, o nome torna-se obrigatório e o NIF, o telefone e o email são opcionais
    elsif (p_id_cliente is null) then
        if (p_nome is null) then
            raise ex_parametro_nome;
        end if;
    end if;

    -- ID Reserva
    select max(id) into v_id_reserva
    from reserva;
    v_id_reserva := v_id_reserva + 1;

    -- Preco
    v_preco := 35;
    select a.preco into v_preco
    from preco_epoca_tipo_quarto a join epoca b on (a.id_epoca = b.id)
    where id_tipo_quarto = p_tipo_quarto and p_data_entrada between b.data_ini and b.data_fim; 

    insert into reserva(id, id_cliente, nome, nif, email, telefone, id_tipo_quarto, data, data_entrada, data_saida, nr_pessoas, preco, id_estado_reserva)
    values (v_id_reserva, p_id_cliente, p_nome, p_nif, p_email, p_telefone, p_tipo_quarto, sysdate, p_data_entrada, p_data_saida, p_nr_pessoas, v_preco, 1);
    dbms_output.put_line('Reserva registada com sucesso.');

exception
    when ex_tipo_quarto_invalido then
        raise_application_error(-20050, 'Tipo de Quarto inválido.');
    when ex_data_invalida then
        raise_application_error(-20051, 'As datas introduzidas não podem ser mais antigas que a data atual.');
    when ex_data_rage_invalida then
        raise_application_error(-20052, 'A data de saída tem que ser posterior à data de entrada.');
    when ex_nr_pessoas_invalido then
        raise_application_error(-20053, 'Número de pessoas inválido.');
    when ex_parametro_id then
        raise_application_error(-20054, 'Quando se especifica o parâmetro id do cliente, não se pode especificar o nome, o NIF, o telefone nem o email.');
    when ex_cliente_invalido then
        raise_application_error(-20055, 'ID de Cliente inválido.');
    when ex_parametro_nome then
        raise_application_error(-20056, 'Quando não se especifica o parâmetro id do cliente, o nome torna-se obrigatório.');
end;
/

-- TESTES

set serveroutput on;

-- Inserir época e preço por época e tipo de quarto para calcular o preço da reserva para janeiro de 2021
declare
  v_preco number;
begin
    insert into epoca(id, nome, data_ini, data_fim) values(10, 'Época Teste', to_date('2021-01-01', 'yyyy-mm-dd'), to_date('2021-03-31', 'yyyy-mm-dd'));
    for tq in (select * from tipo_quarto) loop
        v_preco := mod(10 * tq.id * 10, 20) + 30;
            insert into preco_epoca_tipo_quarto(id_epoca, id_tipo_quarto, preco) values(10, tq.id, v_preco);
        end loop;
    end;
/

-- Exceção Tipo Quarto
declare
ret boolean;
begin
    prcRegistarReserva(-3, to_date('2021-01-01', 'yyyy-mm-dd'), to_date('2021-01-04', 'yyyy-mm-dd'), 4, 3);
end;
/

-- Exceção Data Antiga
declare
ret boolean;
begin
    prcRegistarReserva(2, to_date('2019-01-01', 'yyyy-mm-dd'), to_date('2019-01-04', 'yyyy-mm-dd'), 4, 3);
end;
/

-- Exceção Data Range
declare
ret boolean;
begin
    prcRegistarReserva(2, to_date('2021-01-04', 'yyyy-mm-dd'), to_date('2021-01-01', 'yyyy-mm-dd'), 4, 3);
end;
/

-- Exceção Número de Pessoas
declare
ret boolean;
begin
    prcRegistarReserva(2, to_date('2021-01-01', 'yyyy-mm-dd'), to_date('2021-01-04', 'yyyy-mm-dd'), 0, 3);
end;
/

-- Exceção - Quando se especifica o parâmetro id do cliente, não se pode especificar o nome, o NIF, o telefone nem o email
declare
ret boolean;
begin
    prcRegistarReserva(2, to_date('2021-01-01', 'yyyy-mm-dd'), to_date('2021-01-04', 'yyyy-mm-dd'), 4, 3, 'Nome Teste');
end;
/

-- Exceção - Quando se especifica o parâmetro id do cliente, não se pode especificar o nome, o NIF, o telefone nem o email
declare
ret boolean;
begin
    prcRegistarReserva(2, to_date('2021-01-01', 'yyyy-mm-dd'), to_date('2021-01-04', 'yyyy-mm-dd'), 4, 3, null, 'ayau');
end;
/

-- Exceção - ID de Cliente inválido
declare
ret boolean;
begin
    prcRegistarReserva(2, to_date('2021-01-01', 'yyyy-mm-dd'), to_date('2021-01-04', 'yyyy-mm-dd'), 4, -1);
end;
/

-- Exceção - Quando não se especifica o parâmetro id do cliente, o nome torna-se obrigatório
begin
    prcRegistarReserva(2, to_date('2021-01-01', 'yyyy-mm-dd'), to_date('2021-01-04', 'yyyy-mm-dd'), 4, null);
end;
/

-- Teste - Com ID Cliente
begin
    prcRegistarReserva(2, to_date('2021-01-01', 'yyyy-mm-dd'), to_date('2021-01-04', 'yyyy-mm-dd'), 4, 3);
end;
/
select * from reserva where id = (select max(id) from reserva);

-- Teste - Com Nome de Cliente
begin
    prcRegistarReserva(2, to_date('2021-01-01', 'yyyy-mm-dd'), to_date('2021-01-04', 'yyyy-mm-dd'), 4, null, 'Nome Teste');
end;
/
select * from reserva where id = (select max(id) from reserva);

delete from preco_epoca_tipo_quarto where id_epoca = 10;
delete from epoca where id = 10;