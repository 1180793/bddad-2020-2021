DROP SEQUENCE seq_fatura;

CREATE SEQUENCE seq_fatura
  MINVALUE 1
  MAXVALUE 9999
  START WITH 1
  INCREMENT BY 1
  NOCACHE;

create or replace procedure prcCheckOut(p_idreserva reserva.id%type)
is 
    v_reserva reserva%rowtype;
    v_conta conta_consumo%rowtype;
    v_linha_conta linha_conta_consumo%rowtype;
    v_id_fatura fatura.id%type;
    v_total number(8,2);
    aux number(8,2);
    linha_count number;
    
    cursor v_linhas_conta_consumo(id_conta conta_consumo.id%type) is 
        select *
        from linha_conta_consumo
        where id_conta_consumo = id_conta;
    
    ex_parametro_null exception;
    ex_parametro_inexistente exception;
    ex_parametro_invalido exception;
    
begin
    -- parametro null
	if p_idreserva is null then
		raise ex_parametro_null;
	end if;
	
	-- parametro nao existe
	select count(*) into aux	
	from reserva
	where p_idreserva = id;
	
	if aux = 0 then
		raise ex_parametro_inexistente;
	end if;
	
    -- reserva nao passou pelo checkin
    select id_estado_reserva into aux
    from reserva
    where id = p_idreserva;
    
    if aux!=2 then 
        raise ex_parametro_invalido;
	end if; 

    -- inicializacao
    v_total := 0; 
    aux := 0;
    linha_count := 1;
    
    select * into v_reserva -- reserva a realizar checkout
    from reserva
    where id = p_idreserva;
    
    select * into v_conta -- conta da mesma reserva 
    from conta_consumo
    where id_reserva = p_idreserva; 
    
    aux := v_reserva.preco;
    
    if v_reserva.custo_cancelamento is not null then
        aux := aux + v_reserva.custo_cancelamento; 
    end if;
    
    if v_reserva.custo_extra is not null then
        aux := aux + v_reserva.custo_extra;
    end if;
    
    insert into fatura(id, numero, data, id_cliente, id_reserva, valor_faturado_reserva) 
    values(seq_fatura.nextval, seq_fatura.currval, sysdate, v_reserva.id_cliente, v_reserva.id, aux);
    
    aux := 0;
    
    select id into v_id_fatura -- fatura da conta
    from fatura
    where id_reserva = p_idreserva; 
    
    open v_linhas_conta_consumo(v_conta.id);
        loop
            fetch v_linhas_conta_consumo into v_linha_conta;
            exit when v_linhas_conta_consumo%notfound;
            
            aux := v_linha_conta.preco_unitario * v_linha_conta.quantidade;
            
            insert into linha_fatura(id_fatura, linha, id_conta_consumo, linha_conta_consumo, valor_consumo)
            values(v_id_fatura, linha_count, v_conta.id, v_linha_conta.linha, aux);
            
            linha_count := linha_count + 1;
            v_total := v_total + aux;
            
        end loop;
    close v_linhas_conta_consumo;
    
    -- adiciona valor total de consumos à fatura
    update fatura
    set valor_faturado_consumo = v_total
    where id_reserva = p_idreserva; 
        
    -- altera estado da reserva para finalizada
    update reserva
    set id_estado_reserva = 3
    where id = p_idreserva;

exception		
    when ex_parametro_null then
        raise_application_error(-20000, 'Parametro null!');
		
	when ex_parametro_inexistente then
		raise_application_error(-20001, 'Reserva nao existe!');
	
	when ex_parametro_invalido then
		raise_application_error(-20002, 'Parametro invalido! A reserva nao se encontra em estado de fazer o checkout.');
		
end;
/

-- TESTES

set serveroutput on;

-- Reserva pronta para checkout
declare
begin
    prcCheckOut(8); 
end;
/

select * from fatura where id_reserva = 8;
select * from linha_fatura where id_fatura = (select id from fatura where id_reserva=8);
select id_estado_reserva from reserva where id = 8;


-- Reserva sem checkin
declare
begin
    prcCheckOut(191); 
end;
/

-- Parametro null
declare
begin
    prcCheckOut(null); 
end;
/

-- Reserva nao existe
declare
begin
    prcCheckOut(9999); 
end;
/

