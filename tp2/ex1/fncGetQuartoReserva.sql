create or replace function fncGetQuartoReserva(p_idreserva reserva.id%type) return quarto%rowtype
is	
	v_ret quarto%rowtype;	
	v_reserva reserva%rowtype;
	v_quarto quarto%rowtype;
	v_checkin checkin%rowtype;
	aux number;	

	cursor v_quartos_tipo(p_idtipo reserva.id_tipo_quarto%type, p_nr_pessoas reserva.nr_pessoas%type) is 
        select distinct q.id, q.id_andar, count(*) as qtddReservasAno
        from checkin c
        inner join (
            select *
            from quarto q
            where q.id_tipo_quarto = p_idtipo and q.lotacao_maxima >= p_nr_pessoas 
        ) q on c.id_quarto = q.id
        where c.data > add_months(sysdate, -12)
        group by q.id, q.id_andar 
        order by id_andar asc, qtddReservasAno asc;

    v_aux v_quartos_tipo%rowtype;

	ex_reserva_inexistente exception;
	ex_parametro_invalido exception;
	ex_quarto_associado exception;
	ex_estado_inadequado exception;

begin	
	--a) Reserva NULL
	if p_idreserva is null then
		raise ex_parametro_invalido;
	end if;	

	--b) Reserva não existe
	select count(*) into aux	
	from reserva
	where p_idreserva = id;

	if aux = 0 then
		raise ex_reserva_inexistente;
	end if;

    select * into v_reserva
    from reserva
	where p_idreserva = id;

	--c) Reserva já tem quarto associado
	aux := 0;
	select count(*) into aux	
	from checkin
	where p_idreserva = id_reserva;

	if aux != 0 then
		raise ex_quarto_associado;
	end if;

	--d) Estado da reserva não é válido (diferente de 'ABERTO')
	select id_estado_reserva into aux
	from reserva
	where p_idreserva = id;

	if aux != 1 then --so e valido se a encomenda estiver no estado ABERTA
		raise ex_estado_inadequado;
	end if;


	open v_quartos_tipo(v_reserva.id_tipo_quarto, v_reserva.nr_pessoas);
        loop
            fetch v_quartos_tipo into v_aux;
            exit when v_quartos_tipo%notfound;

            if not isQuartoIndisponivel(v_aux.id, v_reserva.data_entrada) then
                select * into v_ret
                from quarto q
                where q.id = v_aux.id;

                return v_ret;
            end if;
        end loop;
    close v_quartos_tipo;

	return null;

exception		
	when ex_parametro_invalido then
	    raise_application_error(-20010, 'Parametro invalido!');

	when ex_reserva_inexistente then
	    raise_application_error(-20012, 'Reserva inexistente!');

	when ex_quarto_associado then
	    raise_application_error(-20013, 'Reserva já tem um quarto associado!');

	when ex_estado_inadequado then
	    raise_application_error(-20014, 'Estado inadequado!');
end;
/

-- TESTES

set serveroutput on;

-- Exceção NULL
declare
ret quarto%rowtype;
begin
    ret := fncGetQuartoReserva(null);
    if ret.id is null then
        DBMS_OUTPUT.PUT_LINE('Não foi possível atribuir um quarto para a reserva especificada.');
    else
        DBMS_OUTPUT.PUT_LINE('ID Quarto Atribuído: ' || ret.id);
    end if;
end;
/

-- Reserva não existe
declare
ret quarto%rowtype;
begin
    ret := fncGetQuartoReserva(9999);
    if ret.id is null then
        DBMS_OUTPUT.PUT_LINE('Não foi possível atribuir um quarto para a reserva especificada.');
    else
        DBMS_OUTPUT.PUT_LINE('ID Quarto Atribuído: ' || ret.id);
    end if;
end;
/

-- Reserva já tem um quarto associado
declare
ret quarto%rowtype;
begin
    ret := fncGetQuartoReserva(1);
    if ret.id is null then
        DBMS_OUTPUT.PUT_LINE('Não foi possível atribuir um quarto para a reserva especificada.');
    else
        DBMS_OUTPUT.PUT_LINE('ID Quarto Atribuído: ' || ret.id);
    end if;
end;
/

-- Reserva tem um estado diferente de 'ABERTO'
declare
ret quarto%rowtype;
begin
    update reserva set id_estado_reserva = 5 where id = 3643;
    ret := fncGetQuartoReserva(3643);
    if ret.id is null then
        DBMS_OUTPUT.PUT_LINE('Não foi possível atribuir um quarto para a reserva especificada.');
    else
        DBMS_OUTPUT.PUT_LINE('ID Quarto Atribuído: ' || ret.id);
    end if;
    update reserva set id_estado_reserva = 1 where id = 3643;
end;
/


-- Existem cinco quartos com as carateristicas pretendidas (2, 5, 8, 11, 14) ordenados pelo andar e pelo numero de reservas no ultimo ano
-- Os 5 quartos encontram-se disponíveis, irá retornar o primeiro da lista.
-- Query com os detalhes da Reserva - 3643
select *
from reserva
where id = 3643;

-- Query que retorna os quartos que satisfazem as condições da reserva
select distinct q.id, q.id_andar, count(*) as qtddReservasAno
from checkin c
inner join (
    select *
    from quarto q
    where q.id_tipo_quarto = 2 and q.lotacao_maxima >= 4
) q on c.id_quarto = q.id
where c.data > add_months(sysdate, -12)
group by q.id, q.id_andar 
order by id_andar asc, qtddReservasAno asc;

declare
ret quarto%rowtype;
begin
    ret := fncGetQuartoReserva(3643);
    if ret.id is null then
        DBMS_OUTPUT.PUT_LINE('Não foi possível atribuir um quarto para a reserva especificada.');
    else
        DBMS_OUTPUT.PUT_LINE('ID Quarto Atribuído: ' || ret.id);
    end if;
end;
/

-- Existem cinco quartos com as carateristicas pretendidas (2, 5, 8, 11, 14) ordenados pelo andar e pelo numero de reservas no último ano
-- Nenhum dos quartos se encontra disponível para a data especificada na reserva logo irá retornar null.
declare
ret quarto%rowtype;
begin
    update reserva set id_estado_reserva = 1 where id = 270;
    delete from checkout where id_reserva = 270;
    delete from checkin where id_reserva = 270;
    ret := fncGetQuartoReserva(270);
    if ret.id is null then
        DBMS_OUTPUT.PUT_LINE('Não foi possível atribuir um quarto para a reserva especificada.');
    else
        DBMS_OUTPUT.PUT_LINE('ID Quarto Atribuído: ' || ret.id);
    end if;
end;
/