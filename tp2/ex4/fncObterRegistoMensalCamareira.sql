create or replace function fncObterRegistoMensalCamareira(p_mes integer, p_ano integer default (extract(year from sysdate)) - 1)
    return SYS_REFCURSOR
as
    retCursor SYS_REFCURSOR;    

    ex_mes exception;
begin
    if not (p_mes between 1 and 12) then
        raise ex_mes;
    end if;

    open retCursor for
        select c.id, f.nome, sum(lcc.preco_unitario), min(lcc.data_registo), max(lcc.data_registo), (extract(day from last_day(min(lcc.data_registo))) - count(distinct lcc.data_registo))
        from
            camareira             c,
            funcionario           f,
            linha_conta_consumo   lcc
        where
            c.id = f.id
            and lcc.id_camareira = c.id
            and extract(year from lcc.data_registo) = p_ano
            and extract(month from lcc.data_registo) = p_mes
        group by
            c.id,
            f.nome
        order by 1;

    return retCursor;
exception
	when ex_mes then
		raise_application_error(-20020, 'O mês é inválido');
end;
/

-- Testes

set serveroutput on;

-- Testes - Registo Mensal
declare
	retCursor SYS_REFCURSOR;
	
	id camareira.id%type;
	nome funcionario.nome%type;
	total number;
	dataInicio date;
	dataFim date;
	diasSemIntervencoes integer;
begin
	retCursor := fncObterRegistoMensalCamareira(5, 2020);
	loop
		fetch retCursor into id, nome, total, dataInicio, dataFim, diasSemIntervencoes;
		exit when retCursor%NOTFOUND;
		DBMS_OUTPUT.PUT_LINE(id ||'  '|| nome ||'   '||  total||'  '|| dataInicio ||'   ' || dataFim ||'  '|| diasSemIntervencoes);
	end loop;
end;
/

-- Testes - Registo Mensal - Sem registos
declare
	retCursor SYS_REFCURSOR;
	
	id camareira.id%type;
	nome funcionario.nome%type;
	total number;
	dataInicio date;
	dataFim date;
	diasSemIntervencoes integer;
	
	isEmpty boolean;
begin
	isEmpty := true;
	retCursor := fncObterRegistoMensalCamareira(5);
	loop
		fetch retCursor into id, nome, total, dataInicio, dataFim, diasSemIntervencoes;
		exit when retCursor%NOTFOUND;
		dbms_output.put_line(id ||'  '|| nome ||'   '||  total||'  '|| dataInicio ||'   ' || dataFim ||'  '|| diasSemIntervencoes);
		isEmpty := false;
	end loop;
	if isEmpty then
		dbms_output.put_line('Não existem registos para a data inserida.');
	end if;
end;
/