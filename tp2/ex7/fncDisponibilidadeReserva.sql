create or replace function fncDisponibilidadeReserva(p_tipo_quarto tipo_quarto.id%type, p_data date, p_duracao integer, p_nr_pessoas integer)
    return boolean
as
    aux number;
    v_invalido boolean;
    v_data_aux date;
    v_cont integer;
    
    v_return boolean;
    
    cursor v_quartos_possiveis is
        select distinct q.id
        from quarto q
        where q.id_tipo_quarto = p_tipo_quarto and q.lotacao_maxima >= p_nr_pessoas;
        
    v_row v_quartos_possiveis%rowtype;

    ex_tipo_quarto exception;
    ex_data exception;
    ex_duracao exception;
    ex_nr_pessoas exception;
begin
    v_return := false;
    
    -- Validação Tipo Quarto
    select count(*) into aux
    from tipo_quarto tq
    where tq.id = p_tipo_quarto;
    
    if aux = 0 then
        raise ex_tipo_quarto;
    end if;
    
    -- Validação Data
    if (p_data <= sysdate) then
        raise ex_data;
    end if;
    
    -- Validação Duração
    if (p_duracao < 1) then
        raise ex_duracao;
    end if;
    
    -- Validação Número de Pessoas
    if (p_nr_pessoas < 1) then
        raise ex_nr_pessoas;
    end if;
    
    open v_quartos_possiveis;
        -- Para cada quarto
        loop
            fetch v_quartos_possiveis into v_row;
            exit when v_quartos_possiveis%notfound;
                
            v_data_aux := p_data;
            v_invalido := false;
            v_cont := 0;
            -- Para cada dia
            loop
                exit when v_cont = p_duracao;
                if isQuartoIndisponivel(v_row.id, v_data_aux) then
                    v_invalido := true;
                end if;
                
                v_data_aux := v_data_aux + 1;
                v_cont := v_cont + 1;
            end loop;
            
            if v_invalido = false then
                v_return:= true;
            end if;
            
        end loop;
    close v_quartos_possiveis;
    
    return v_return;
exception
    when ex_tipo_quarto then
        -- raise_application_error(-20020, 'O tipo de quarto é inválido');
        return null;
        
    when ex_data then
        -- raise_application_error(-20020, 'A data inserida tem de ser superior à data atual.');
        return null;
        
    when ex_duracao then
        -- raise_application_error(-20020, 'A duração não pode ser um valor inferior a 1.');  
        return null;
        
    when ex_nr_pessoas then
        -- raise_application_error(-20020, 'O número de pessoas não pode ser um valor inferior a 1.');  
        return null;
end;
/

-- TESTES

set serveroutput on;

-- Exceção Tipo Quarto
declare
ret boolean;
begin
    ret := fncDisponibilidadeReserva(-313, to_date('25/12/2020', 'dd/mm/yyyy'), 2, 4);
    if ret = true then
        dbms_output.put_line('Existe disponibilidade.');
    elsif ret = false then
        dbms_output.put_line('Não existe disponibilidade.');
    else 
        dbms_output.put_line('Um dos parametros é inválido.');
    end if;
end;
/

-- Exceção Data
declare
ret boolean;
begin
    ret := fncDisponibilidadeReserva(1, to_date('25/12/2019', 'dd/mm/yyyy'), 2, 4);
    if ret = true then
        dbms_output.put_line('Existe disponibilidade.');
    elsif ret = false then
        dbms_output.put_line('Não existe disponibilidade.');
    else 
        dbms_output.put_line('Um dos parametros é inválido.');
    end if;
end;
/

-- Exceção Duração
declare
ret boolean;
begin
    ret := fncDisponibilidadeReserva(1, to_date('25/12/2020', 'dd/mm/yyyy'), 0, 4);
    if ret = true then
        dbms_output.put_line('Existe disponibilidade.');
    elsif ret = false then
        dbms_output.put_line('Não existe disponibilidade.');
    else 
        dbms_output.put_line('Um dos parametros é inválido.');
    end if;
end;
/

-- Exceção Número de Pessoas
declare
ret boolean;
begin
    ret := fncDisponibilidadeReserva(1, to_date('25/12/2020', 'dd/mm/yyyy'), 2, 0);
    if ret = true then
        dbms_output.put_line('Existe disponibilidade.');
    elsif ret = false then
        dbms_output.put_line('Não existe disponibilidade.');
    else 
        dbms_output.put_line('Um dos parametros é inválido.');
    end if;
end;
/

-- Teste - Disponibilidade
declare
ret boolean;
begin
    ret := fncDisponibilidadeReserva(2, to_date('25/12/2020', 'dd/mm/yyyy'), 2, 4);
    if ret = true then
        dbms_output.put_line('Existe disponibilidade.');
    elsif ret = false then
        dbms_output.put_line('Não existe disponibilidade.');
    else 
        dbms_output.put_line('Um dos parametros é inválido.');
    end if;
end;
/

-- Quartos Possíveis
select distinct q.id
from quarto q
where q.id_tipo_quarto = 2 and q.lotacao_maxima >= 4;

-- Teste - Sem Disponibilidade
declare
ret boolean;
begin
    insert into reserva(id, id_cliente, nome, id_tipo_quarto, data, data_entrada, data_saida, nr_pessoas, preco, id_estado_reserva)
      values(5000, 50, 'Reserva Teste 1', 1, to_date('24/01/2020', 'dd/mm/yyyy'), to_date('24/12/2020', 'dd/mm/yyyy'), to_date('27/01/2020', 'dd/mm/yyyy'), 3, 105, 1);
    insert into checkin(id_reserva, data, id_quarto) values (5000, to_date('24/12/2020', 'dd/mm/yyyy'), 1);
    insert into checkout(id_reserva, data) values (5000, to_date('27/12/2020', 'dd/mm/yyyy'));
    
    insert into reserva(id, id_cliente, nome, id_tipo_quarto, data, data_entrada, data_saida, nr_pessoas, preco, id_estado_reserva)
      values(5001, 50, 'Reserva Teste 2', 1, to_date('24/01/2020', 'dd/mm/yyyy'), to_date('24/12/2020', 'dd/mm/yyyy'), to_date('27/01/2020', 'dd/mm/yyyy'), 3, 105, 1);
    insert into checkin(id_reserva, data, id_quarto) values (5001, to_date('24/12/2020', 'dd/mm/yyyy'), 4);
    insert into checkout(id_reserva, data) values (5001, to_date('27/12/2020', 'dd/mm/yyyy'));
    
    insert into reserva(id, id_cliente, nome, id_tipo_quarto, data, data_entrada, data_saida, nr_pessoas, preco, id_estado_reserva)
      values(5002, 50, 'Reserva Teste 3', 1, to_date('24/01/2020', 'dd/mm/yyyy'), to_date('24/12/2020', 'dd/mm/yyyy'), to_date('27/01/2020', 'dd/mm/yyyy'), 3, 105, 1);
    insert into checkin(id_reserva, data, id_quarto) values (5002, to_date('24/12/2020', 'dd/mm/yyyy'), 7);
    insert into checkout(id_reserva, data) values (5002, to_date('27/12/2020', 'dd/mm/yyyy'));
    
    ret := fncDisponibilidadeReserva(1, to_date('24/12/2020', 'dd/mm/yyyy'), 1, 3);
    if ret = true then
        dbms_output.put_line('Existe disponibilidade.');
    elsif ret = false then
        dbms_output.put_line('Não existe disponibilidade.');
    else 
        dbms_output.put_line('Um dos parametros é inválido.');
    end if;
    
    delete from checkout where id_reserva = 5000;
    delete from checkout where id_reserva = 5001;
    delete from checkout where id_reserva = 5002;
    delete from checkin where id_reserva = 5000;
    delete from checkin where id_reserva = 5001;
    delete from checkin where id_reserva = 5002;
    delete from reserva where id = 5000;
    delete from reserva where id = 5001;
    delete from reserva where id = 5002;
end;
/

-- Quartos Possíveis
select distinct q.id
from quarto q
where q.id_tipo_quarto = 1 and q.lotacao_maxima >= 3;