create or replace trigger trgAtualizaCliente before insert or update on reserva for each row
declare
	v_aux number;
    v_cliente_row cliente%rowtype;
    
    ex_cliente_invalido exception;
begin
	if :new.id_cliente is not null then
        select count(*) into v_aux
        from cliente
        where id = :new.id_cliente;
        
        if v_aux = 0 then
            raise ex_cliente_invalido;
        else
            select * into v_cliente_row
            from cliente
            where id = :new.id_cliente;
            
            :new.nome := v_cliente_row.nome;
            :new.nif := v_cliente_row.nif;
            :new.email := v_cliente_row.email;
            :new.telefone := v_cliente_row.telefone;
        end if;
    end if;
exception
    when ex_cliente_invalido then
        raise_application_error(-20060, 'O ID de Cliente introduzido na reserva é inválido.');
end;
/

-- TESTES

set serveroutput on;

-- Teste - Cliente Inválido
declare
    v_id_reserva reserva.id%type;
    
begin
    update cliente set nif = 'NIF Teste', telefone = '+351912345678', email = 'email_teste@isep.ipp.pt' where id = 4;
    select max(id) into v_id_reserva
    from reserva;
    v_id_reserva := v_id_reserva + 1;
    
    insert into reserva(id, id_cliente, nome, nif, email, telefone, id_tipo_quarto, data, data_entrada, data_saida, nr_pessoas, preco, id_estado_reserva)
        values (v_id_reserva, -10, null, null, null, null, 2, sysdate, to_date('2021-01-04', 'yyyy-mm-dd'), to_date('2021-01-07', 'yyyy-mm-dd'), 4, 50, 1);
end;
/

-- Teste - Insert - Atualiza Nome, NIF, Telefone e Email
declare
    v_id_reserva reserva.id%type;
    
begin
    update cliente set nif = 'NIF Teste', telefone = '+351912345678', email = 'email_teste@isep.ipp.pt' where id = 4;
    select max(id) into v_id_reserva
    from reserva;
    v_id_reserva := v_id_reserva + 1;
    
    insert into reserva(id, id_cliente, nome, nif, email, telefone, id_tipo_quarto, data, data_entrada, data_saida, nr_pessoas, preco, id_estado_reserva)
        values (v_id_reserva, 4, null, null, null, null, 2, sysdate, to_date('2021-01-04', 'yyyy-mm-dd'), to_date('2021-01-07', 'yyyy-mm-dd'), 4, 50, 1);
end;
/

select * from cliente where id = 4;
select * from reserva where id = (select max(id) from reserva);

-- Teste - Update - Atualiza Nome, NIF, Telefone e Email
select * from reserva where id = 50;
    
begin
    update cliente set nif = 'NIF Test Update', telefone = '+351987654321', email = 'email_teste2@isep.ipp.pt' where id = 5;
    
    update reserva set id_cliente = 5 where id = 50;
end;
/

select * from cliente where id = 5;
select * from reserva where id = 50;
