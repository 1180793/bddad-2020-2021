create or replace trigger trgEpocasNaoSobrepostas before insert or update on epoca
for each row
declare
    pragma autonomous_transaction; -- Necessário para o update porque será necessário aceder aos registos da tabela em que o trigger foi executado 
	aux number;
    
    ex_sobreposicao exception;
begin
    if inserting then
        select count(*) into aux
        from epoca
        where data_ini <= :new.data_fim and data_fim >= :new.data_ini;
    
        if aux > 0 then
            raise ex_sobreposicao;  
        end if;
    end if;
    if updating then
        select count(*) into aux
        from epoca
        where id != :new.id and data_ini <= :new.data_fim and data_fim >= :new.data_ini; 
        
        if aux > 0 then
            raise ex_sobreposicao;  
        end if;
    end if;
    
    commit;
exception
    when ex_sobreposicao then
        raise_application_error(-20018, 'A nova época introduzida sobrepõe uma época existente');
end;
/

-- TESTES

-- Teste - As épocas inseridas na base de dados ocupam o ano todo de 2020 pelo que será impossível inserir uma nova época neste ano
insert into epoca(id, nome, data_ini, data_fim) values (5, 'Época 5', to_date('19/11/2020', 'dd/mm/yyyy'), to_date('21/11/2020', 'dd/mm/yyyy'));

-- Teste - Inserir uma época no ano de 2021 (que ainda não tem épocas definidas)
insert into epoca(id, nome, data_ini, data_fim) values (6, 'Época 6', to_date('01/02/2021', 'dd/mm/yyyy'), to_date('01/03/2021', 'dd/mm/yyyy'));

-- Teste - Sobreposição de Épocas - Data Inicio no meio de uma Época existente
insert into epoca(id, nome, data_ini, data_fim) values (7, 'Época 7', to_date('15/02/2021', 'dd/mm/yyyy'), to_date('01/04/2021', 'dd/mm/yyyy'));

-- Teste - Sobreposição de Épocas - Data Fim no meio de uma Época existente
insert into epoca(id, nome, data_ini, data_fim) values (8, 'Época 8', to_date('01/01/2021', 'dd/mm/yyyy'), to_date('15/02/2021', 'dd/mm/yyyy'));

-- Teste - Sobreposição de Épocas - Data Inicio e Data Fim no meio de uma Época existente
insert into epoca(id, nome, data_ini, data_fim) values (9, 'Época 9', to_date('14/02/2021', 'dd/mm/yyyy'), to_date('17/02/2021', 'dd/mm/yyyy'));

-- Teste - Sobreposição de Épocas - Época no meio da Data Inicio e Data Fim inseridas
insert into epoca(id, nome, data_ini, data_fim) values (10, 'Época 10', to_date('14/01/2021', 'dd/mm/yyyy'), to_date('17/04/2021', 'dd/mm/yyyy'));

-- Teste - Sem Sobreposição de Épocas
insert into epoca(id, nome, data_ini, data_fim) values (11, 'Época 11', to_date('01/04/2021', 'dd/mm/yyyy'), to_date('17/05/2021', 'dd/mm/yyyy'));


-- Teste - Update sem sobreposição
update epoca set data_ini = to_date('01/12/2019', 'dd/mm/yyyy') where id = 1;
update epoca set data_ini = to_date('01/01/2020', 'dd/mm/yyyy') where id = 1;

-- Teste - Update com sobreposição
update epoca set data_ini = to_date('01/04/2020', 'dd/mm/yyyy') where id = 3;

