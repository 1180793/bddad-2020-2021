-- Alterações Modelo Relacional
drop table camareira_bonus cascade constraints purge;

-- Camareira Bonus
create table camareira_bonus (
    id_camareira    integer         constraint  pk_camareira_bonus_id_camareira primary key,
    bonus 	        number(5,2)		constraint  nn_camareira_bonus_bonus 	    not null
                                    constraint  ck_camareira_bonus_bonus        check(bonus between 0 and 100)
);

alter table camareira_bonus add constraint fk_camareira_bonus_id    foreign key(id_camareira) references camareira(id);

-- Procedures
create or replace procedure prcAtualizarBonusCamareiras(p_mes integer, p_ano integer default (extract(year from sysdate)))
is
    aux     number;
    v_bonus   number;
    cursor c is 
        select c.id, f.nome, sum(lcc.preco_unitario) as total_consumo
        from
            camareira             c,
            funcionario           f,
            linha_conta_consumo   lcc
        where
            c.id = f.id
            and lcc.id_camareira = c.id
            and extract(year from lcc.data_registo) = p_ano
            and extract(month from lcc.data_registo) = p_mes
        group by
            c.id,
            f.nome;
    r c%rowtype;

    ex_mes exception;
begin
    if not (p_mes between 1 and 12) then
        raise ex_mes;
    end if;

    open c;
    loop
        fetch c into r;
        exit when c%notfound;

        if r.total_consumo > 1000 then
            v_bonus := 15;
        elsif r.total_consumo >= 500 then 
            v_bonus := 10;
        elsif r.total_consumo > 100 then 
            v_bonus := 5;
        else
            v_bonus := 0;
        end if;

        select count(*) into aux	
        from camareira_bonus cb
        where cb.id_camareira = r.id;

        -- Verifica se o registo já existe ou não para saber o tipo de comando DML a executar (insert ou update)
        if aux = 0 then
            insert into camareira_bonus(id_camareira, bonus) values (r.id, v_bonus);
        else
            update camareira_bonus set bonus = v_bonus where id_camareira = r.id;
        end if;
    end loop;
    close c;

exception
    when ex_mes then
        raise_application_error(-20030, 'O mês é inválido');
end;
/

-- TESTES

set serveroutput on;

select * from camareira_bonus;
delete from camareira_bonus;

-- Teste - Mês inválido
begin
    prcAtualizarBonusCamareiras(0, 2020);
end;
/

-- Teste - A tabela que se encontrava vazia vai ser atualizada com os bónus atribuídos às camareiras de acordo com os consumos registados no mês e ano especificados. (insert)
begin
    prcAtualizarBonusCamareiras(5);
end;
/

-- Teste - A tabela já se encontrava preenchida, será reescrita com os valores a relativos a outro mês. Atualizamos o valor de uma das camareiras para ultrapassar os 1000 de modo a ser atribuído o bónus de 15%
begin
    update linha_conta_consumo set preco_unitario = 1500 where id_conta_consumo = 915 and linha = 1;
    prcAtualizarBonusCamareiras(3, 2020);
    update linha_conta_consumo set preco_unitario = 1 where id_conta_consumo = 915 and linha = 1;
end;
/