-- PL14
-- 5. Criar um script com código PL/SQL para implementar um procedimento, designado prcUniformizarStockMinEditora, para nivelar,
--   pelo maior valor, o stock mínimo de todas as edições de livros de uma dada editora. O procedimento deve receber por parâmetro
--   o identificador da Editora e tirar o máximo proveito do SQL. Testar adequadamente o procedimento implementado através de
--   blocos anónimos

SET SERVEROUTPUT ON;

SELECT el.isbn, MAX(el.stockMin)
  FROM EDICAOLIVRO el
 WHERE el.idEditora = 1500
 GROUP BY el.isbn;

SELECT MAX(el.stockMin)
  FROM EDICAOLIVRO el
 WHERE el.idEditora = 1500;

create or replace procedure prcUniformizarStockMinEditora(p_ideditora editora.idEditora%type)
is
begin
  update edicaoLivro
     set stockmin = (SELECT max(stockMin)
                       FROM edicaoLivro
                      WHERE idEditora = p_idEditora)
   where ideditora = p_ideditora;
end;
/

SET SERVEROUTPUT ON;
SELECT * 
FROM edicaoLivro 
WHERE ideditora = 1500;

BEGIN
  prcUniformizarStockMinEditora(1500);  
END;
/

-- 6. Criar um novo script com código PL/SQL para implementar um procedimento, designado prcNovoPrecoProximaSemanaLivrosEmIngles, 
--   para inserir um novo preço nas edições de livros em idioma inglês, a aplicar a partir de um dado dia da próxima semana (i.e. 
--   semana a seguir à semana da data atual). O procedimento deve receber por parâmetro, o dia de semana (e.g. 4) e a percentagem 
--   de redução (e.g. -20) ou aumento (e.g. 20) do preço atual. O dia de semana deve ser representado por um número inteiro entre
--   um (i.e. domingo) e sete (i.e. sábado). A data desse dia da semana pode ser obtida através da função NEXT_DAY. O procedimento
--   deve validar o dia da semana e, no caso de ser inválido, deve levantar uma exceção própria e fornecer uma mensagem apropriada.
--   O procedimento deve recorrer a um CURSOR explícito e às instruções OPEN, FETCH e CLOSE para processar o CURSOR, devendo tirar
--   o máximo proveito do SQL. Testar adequadamente o procedimento implementado através de blocos anónimos.
SELECT * 
FROM idioma;

SELECT a.isbn, b.preco
  FROM edicaoLivro a
  JOIN precoedicaolivro b ON (a.isbn = b.isbn)
  JOIN (SELECT isbn, MAX(dataInicio) data
          FROM precoEdicaoLivro
         GROUP BY isbn) c ON (a.isbn = c.isbn)
 WHERE a.codIdioma = 'EN'
   AND b.dataInicio = c.data
 ORDER BY a.isbn;
 
SELECT next_day(TRUNC(sysdate), 7)
FROM dual;

create or replace procedure prcNovoPrecoProximaSemanaLivro(p_dia int, p_pct number)
is
  v_dia date;
  ex_dia exception;
  ex_pct exception;
  
  cursor c is
    select a.isbn, b.preco
      from edicaolivro a
      join precoedicaolivro b on (a.isbn = b.isbn)
      join (select isbn, max(datainicio) data
              from precoedicaolivro
             group by isbn) c on (a.isbn = c.isbn)
     where a.codidioma = 'EN'
       and b.datainicio = c.data
     order by a.isbn;
  r c%rowtype;
begin
  if not (p_dia between 1 and 7) then
    raise ex_dia;
  end if;
  if not (p_pct between -100 and 100) then
    raise ex_pct;
  end if;
  --Próximo dia do tipo pretendido.
  --v_dia := next_day(trunc(sysdate), 1);
  select next_day(trunc(sysdate), p_dia) into v_dia from dual;
  --dbms_output.put_line(v_dia);
  
  open c;
  loop
    fetch c into r;
    exit when c%notfound;
    --
    insert into precoedicaolivro(isbn, datainicio, preco)
    values(r.isbn, v_dia, r.preco * (100+p_pct)/100);
  end loop;
  close c;
exception
  when ex_dia then
    raise_application_error(-20000, 'O dia da semana é inválido');
  when ex_pct then
    raise_application_error(-20001, 'A percentagem é um valor entre -100 e 100');
end;
/

select * from precoedicaolivro where isbn = '978-1137279170';
begin
  prcNovoPrecoProximaSemanaLivro(2, 20);
end;
/

select * from edicaolivro
 where isbn = '500-1234567891';

select * from precoedicaolivro
 where isbn = '500-123456789';

select 27*1.2 from dual;
/

-- 7. Duplicar o script anterior e alterar o código de modo a processar o CURSOR com uma instrução FOR de CURSOR. Testar
--   adequadamente o procedimento implementado através de blocos anónimos.



create or replace procedure prcNovoPrecoProximaSemanaLivro(p_dia int, p_pct number)
is
  v_dia date;
  ex_dia exception;
  ex_pct exception;
  
  cursor c is
    select a.isbn, b.preco
      from edicaolivro a
      join precoedicaolivro b on (a.isbn = b.isbn)
      join (select isbn, max(datainicio) data
              from precoedicaolivro
             group by isbn) c on (a.isbn = c.isbn)
     where a.codidioma = 'EN'
       and b.datainicio = c.data
     order by a.isbn;
begin
  if not (p_dia between 1 and 7) then
    raise ex_dia;
  end if;
  if not (p_pct between -100 and 100) then
    raise ex_pct;
  end if;
  --Próximo dia do tipo pretendido.
  --v_dia := next_day(trunc(sysdate), 1);
  select next_day(trunc(sysdate), p_dia) into v_dia from dual;
  --dbms_output.put_line(v_dia);

  for r in c
  loop
    insert into precoedicaolivro(isbn, datainicio, preco)
    values(r.isbn, v_dia, r.preco * (100+p_pct)/100); 
  end loop;
  
exception
  when ex_dia then
    raise_application_error(-20000, 'O dia da semana é inválido');
  when ex_pct then
    raise_application_error(-20001, 'A percentagem é um valor entre 0 e 100');
end;
/

select * from precoedicaolivro where isbn = '978-1137279170';
begin
  prcNovoPrecoProximaSemanaLivro(3, 20);
end;
/

-- 8. Duplicar o script anterior e substituir a instrução FOR de CURSOR por uma instrução FOR de QUERY. Testar adequadamente o 
--   procedimento implementado através de blocos anónimos.
create or replace procedure prcNovoPrecoProximaSemanaLivro(p_dia int, p_pct number)
is
  v_dia date;
  ex_dia exception;
  ex_pct exception;
begin
  if not (p_dia between 1 and 7) then
    raise ex_dia;
  end if;
  if not (p_pct between -100 and 100) then
    raise ex_pct;
  end if;
  --Próximo dia do tipo pretendido.
  --v_dia := next_day(trunc(sysdate), 1);
  select next_day(trunc(sysdate), p_dia) into v_dia from dual;
  --dbms_output.put_line(v_dia);

  for r in (select a.isbn, b.preco
      from edicaolivro a
      join precoedicaolivro b on (a.isbn = b.isbn)
      join (select isbn, max(datainicio) data
              from precoedicaolivro
             group by isbn) c on (a.isbn = c.isbn)
     where a.codidioma = 'EN'
       and b.datainicio = c.data
     order by a.isbn)
  loop
    insert into precoedicaolivro(isbn, datainicio, preco)
    values(r.isbn, v_dia, r.preco * (100+p_pct)/100); 
  end loop;
  
exception
  when ex_dia then
    raise_application_error(-20000, 'O dia da semana é inválido');
  when ex_pct then
    raise_application_error(-20001, 'A percentagem é um valor entre 0 e 100');
end;
/
