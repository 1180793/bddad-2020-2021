-- PL07
-- 1. Mostrar todos os dados da tabela CD
SELECT *
FROM CD;

-- 2. Mostrar todos os dados da tabela Musica.
SELECT *
FROM MUSICA;

-- A. Projeção
-- 1. Mostrar o título e a data de compra de todos os CD
SELECT cd.titulo, cd.dataCompra
FROM CD cd;

-- 2. Mostrar a data de compra de todos CD
SELECT cd.dataCompra
FROM CD cd;

-- 3. Mostrar o resultado da alínea anterior, mas sem registos repetidos
SELECT DISTINCT cd.dataCompra
FROM CD cd;

-- 4. Mostrar o código dos CD e os respetivos intérpretes, sem registos repetidos
SELECT DISTINCT m.codCd, m.interprete
FROM MUSICA m;

-- 5. Mostrar o resultado anterior com a primeira coluna intitulada "Código do CD"
SELECT DISTINCT m.codCd AS "Código do CD", m.interprete
FROM MUSICA m;

-- 6. Mostrar o título, o valor pago e o respetivo valor do IVA de todos os CD. O valor do IVA é calculado de acordo com a 
--   seguinte fórmula: valor do IVA = (valor pago * 0.23) / 1.23.
SELECT cd.titulo, cd.valorPago, TRUNC(((valorpago * 0.23)/1.23), 2) AS "Valor do IVA"
FROM CD cd;

-- B. Seleção
-- 1. Mostrar todos os dados das músicas do CD com o código 2;
SELECT *
FROM MUSICA m
WHERE m.codCd = 2;

-- 2. Mostrar todos os dados das músicas que não pertencem ao CD com o código 2
SELECT *
FROM MUSICA m
WHERE m.codCd != 2;

-- 3. Mostrar todos os dados das músicas do CD com o código 2 cuja duração seja superior a 5
SELECT *
FROM MUSICA m
WHERE m.codCd = 2 AND m.duracao > 5;

-- 4. Mostrar todos os dados das músicas do CD com o código 2 cuja duração pertença ao intervalo [4,6]
SELECT *
FROM MUSICA m
WHERE m.codCd = 2 AND m.duracao >= 4 AND m.duracao <= 6;

-- 5. Mostrar todos os dados das músicas do CD com o código 2 cuja duração seja inferior a 4 ou superior a 6
SELECT *
FROM MUSICA m
WHERE m.codCd = 2 AND (m.duracao > 6 OR m.duracao < 4);

-- 6. Mostrar todos os dados das músicas com o número: 1, 3, 5 ou 6
SELECT *
FROM musica m
WHERE m.nrMusica = 1 OR m.nrMusica = 3 OR m.nrMusica = 5 OR m.nrMusica = 6;

-- 7. Mostrar todos os dados das músicas com o número diferente de 1, 3, 5 e 6
SELECT *
FROM musica m
WHERE m.nrMusica != 1 AND m.nrMusica != 3 AND m.nrMusica != 5 AND m.nrMusica != 6;

-- 8. Mostrar todos os dados das músicas cujo intérprete é uma orquestra
SELECT * FROM musica m
WHERE LOWER(m.interprete) LIKE '%orquestra%';

-- 9. Mostrar todos os dados das músicas cujo nome do intérprete tem a letra Y
SELECT * FROM musica m
WHERE LOWER(m.interprete) LIKE '%y%';

-- 10. Mostrar todos os dados das músicas cujo nome termina com DAL?, sendo ? qualquer caráter
SELECT *
FROM musica m
WHERE LOWER(m.titulo) LIKE '%dal_';

-- 11. Mostrar todos os dados das músicas cujo título tem o caráter %
SELECT *
FROM musica m
WHERE m.titulo LIKE '%\%%' ESCAPE '\';

-- 12. Mostrar todos os dados das músicas cujo título é iniciado pela letra B, D ou H
SELECT *
FROM musica m
WHERE REGEXP_LIKE(m.titulo, '^[bdh]', 'i');

-- 13. Mostrar todos os dados dos CD sem o local de compra registado
SELECT *
FROM CD cd
WHERE cd.localCompra IS NULL;

-- 14. Mostrar todos os dados dos CD com o local de compra registado
SELECT *
FROM CD cd
WHERE cd.localCompra IS NOT NULL;

-- C. Projeção e Seleção
-- 1. Mostrar os títulos dos CD comprados na FNAC
SELECT cd.titulo
FROM CD cd
WHERE LOWER(cd.localCompra) LIKE 'fnac';

-- 2. Mostrar os títulos dos CD que não foram comprados na FNAC
SELECT cd.titulo
FROM CD cd
WHERE cd.localCompra IS NULL OR LOWER(cd.localCompra) NOT LIKE 'fnac';

-- D. Ordenação
-- 1. Mostrar os títulos dos CD que não foram comprados na FNAC, por ordem alfabética inversa
SELECT cd.titulo
FROM CD cd
WHERE cd.localCompra IS NULL OR LOWER(cd.localCompra) NOT LIKE 'fnac'
ORDER BY cd.titulo DESC;

-- 2. Mostrar o título e a data de compra dos CD, por ordem alfabética do título do CD
SELECT cd.titulo, cd.dataCompra
FROM CD cd
ORDER BY cd.titulo ASC;

-- 3. Mostrar o título e a data de compra dos CD, por ordem descendente da data de compra do CD
SELECT cd.titulo, cd.dataCompra
FROM CD cd
ORDER BY cd.dataCompra DESC;

-- 4. Mostrar o título e o local de compra dos CD, por ordem ascendente do local de compra do CD
SELECT cd.titulo, cd.localCompra
FROM CD cd
ORDER BY cd.localCompra ASC;

-- 5.  Mostrar o resultado da alínea anterior, mas por ordem descendente do local de compra do CD
SELECT cd.titulo, cd.localCompra
FROM CD cd
ORDER BY cd.localCompra DESC;

-- 6. Mostrar o título, o valor pago e o respetivo valor do IVA dos CD, por ordem decrescente do IVA
SELECT cd.titulo, cd.valorPago, TRUNC(((valorpago * 0.23)/1.23), 2) AS "VALOR_IVA"
FROM CD cd
ORDER BY valor_iva DESC;

-- 7. Mostrar o título do CD por ordem descendente da data de compra e, no caso da igualdade de datas, por ordem alfabética 
--   do título.
SELECT cd.titulo, cd.dataCompra
FROM CD cd
ORDER BY cd.dataCompra DESC, cd.titulo ASC;

-- E. Funções de agregação
-- 1. Mostrar a quantidade de músicas
SELECT COUNT(*) AS "QTDD_MUSICAS"
FROM CD;

-- 2. Mostrar a quantidade de locais de compra distintos
SELECT COUNT(DISTINCT cd.localCompra) AS "QTDD_LOCAIS_COMPRA"
FROM CD cd;

-- 3. Mostrar o total gasto com a compra de todos os CD, o maior e o menor valor pago por um CD
SELECT SUM(cd.valorPago) AS "TOTAL", MAX(cd.valorPago) AS "MAIOR", MIN(cd.valorPago) AS "MENOR"
FROM CD cd;

-- 4. Mostrar a média da duração de todas as músicas
SELECT TRUNC(AVG(m.duracao), 2) AS "MÉDIA"
FROM MUSICA m;

-- 5. Mostrar o total do valor pago na FNAC
SELECT SUM(cd.valorPago) AS "TOTAL_FNAC"
FROM CD cd
WHERE LOWER(cd.localCompra) LIKE 'fnac';

-- 6. Mostrar a diferença entre o maior e o menor valor pago na FNAC
SELECT (MAX(cd.valorPago) - MIN(cd.valorPago)) AS "DIFERENCA_FNAC"
FROM CD cd
WHERE LOWER(cd.localCompra) LIKE 'fnac';