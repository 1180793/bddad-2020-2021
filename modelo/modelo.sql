-- Questão Aula Exemplo

-- 1. Escreva um comando de SQL que permita listar por época, o nome e o preço dos tipos de quartos que possuem preço superior
--   ao preço médio por época.

-- Auxiliar: Preço médio por Época
with aux as (
    select petq.id_epoca, avg(petq.preco) as preco
    from preco_epoca_tipo_quarto petq
    group by petq.id_epoca
    order by 1
)
select e.nome, tq.nome, petq.preco
from preco_epoca_tipo_quarto petq
join epoca e
on petq.id_epoca = e.id
join tipo_quarto tq
on petq.id_tipo_quarto = tq.id
join aux a
on petq.id_epoca = a.id_epoca
where petq.preco > (a.preco)
order by 1, 2;

-- 2. Escreva uma função que retorne o número de vezes que o preço de um dado tipo de quarto sofreu redução entre duas épocas 
--   consecutivas. O identificador do tipo de quarto é fornecido por parâmetro. A função deve retornar o valor null se o 
--   identificador do tipo de quarto não existir.
create or replace function fncNrReducaoPreco(p_id_tipo_quarto tipo_quarto.id%type)
    return number
as
    v_aux number;
    v_cont number;
    cursor v_cursor is
        select *
        from preco_epoca_tipo_quarto petq
        where petq.id_tipo_quarto = p_id_tipo_quarto
        order by 1;
        
    v_anterior v_cursor%rowtype;
    v_atual v_cursor%rowtype;
    
    ex_tipo_quarto_invalido exception;
begin
    v_cont := 0;
    
    select count(*) into v_aux
    from tipo_quarto tq
    where tq.id = p_id_tipo_quarto;
    
    if v_aux = 0 then
        raise ex_tipo_quarto_invalido;
    end if;
    
    open v_cursor;
        loop
            exit when v_cursor%notfound;
            if v_anterior.id_epoca is null then
                fetch v_cursor into v_anterior;
            else
                fetch v_cursor into v_atual;
                if v_atual.preco < v_anterior.preco then
                    v_cont := v_cont + 1;
                end if;
                v_anterior := v_atual;
            end if;
        end loop;
    close v_cursor;
    
    return v_cont;
    
exception
    when ex_tipo_quarto_invalido then
        return null;
end;
/

-- Testes da Função
set serveroutput on;

-- Quarto Inválido
declare
    v_res number;
begin
    v_res := fncNrReducaoPreco(-3);
    if v_res is null then
        DBMS_OUTPUT.PUT_LINE('ID Tipo Quarto Inválido.');
    else
        DBMS_OUTPUT.PUT_LINE('Resultado: ' || v_res);
    end if;
end;
/

-- Quarto Válido
declare
    v_res number;
begin
    v_res := fncNrReducaoPreco(3);
    if v_res is null then
        DBMS_OUTPUT.PUT_LINE('ID Tipo Quarto Inválido');
    else
        DBMS_OUTPUT.PUT_LINE('Resultado: ' || v_res);
    end if;
end;
/