-- PL13
-- 5. Criar um script com código PL/SQL para implementar uma função, designada fncTotalClientes, para retornar o número total de
--   clientes. Caso não existam clientes registados, a função deve retornar o valor NULL. Testar adequadamente a função implementada,
--   através de blocos anónimos. Para visualizar o resultado, executar o comando SET SERVEROUTPUT ON.

SET SERVEROUTPUT ON;

-- SQL QUERY
SELECT COUNT(*) 
FROM CLIENTE;

-- PLSQL
create or replace function fncTotalClientes return integer
is
  v_ret INTEGER;
begin
  SELECT COUNT(*) INTO v_ret
    FROM CLIENTE;
  if v_ret = 0 then
    return null;
  else
    return v_ret;
  end if;
end;
/

-- TESTES
SET SERVEROUTPUT ON;
declare
  v_count int;
begin
  v_count := fncTotalClientes;
  dbms_output.put_line('O número total de clientes é ' || v_count);
end;
/

-- 6. Criar um novo script PL/SQL para implementar uma função, designada fncTemLivrosEditora, para verificar se existem livros de
--   uma dada editora em stock. A função deve receber, por parâmetro, identificador da editora e tem de retornar um valor booleano,
--   true ou false. Se o parâmetro fornecido for inválido, a função deve retornar o valor NULL, usando o mecanismo de exceções. 
--   Testar adequadamente a função implementada, através de blocos anónimos.
create or replace function fncTemLivrosEditora(p_ideditora editora.ideditora%type) return boolean
is
  v_ret int;
  
  ex_editora_inexistente exception;
begin
  --Se o parâmetro fornecido for inválido... (considero que ser inválido é a editora não existir)
  SELECT COUNT(*) into v_ret
    FROM EDITORA e
   where e.idEditora = p_idEditora;
  if v_ret = 0 then
    raise ex_editora_inexistente; -- Não existe nenhuma editora com o id passado por parametro
  end if;
  --
  SELECT COUNT(*) into v_ret
    FROM EDICAOLIVRO el
   where el.idEditora = p_idEditora;
  if v_ret = 0 then
    return false;
  else
    return true;
  end if;
exception
  when ex_editora_inexistente then
    return null;
end;
/

-- TESTES
SET SERVEROUTPUT ON;
select * from editora;
select * from edicaolivro;
-- EDITORA NAO EXISTE
declare
  v_tem_livros boolean;
begin
  v_tem_livros := fncTemLivrosEditora(1501);
  if v_tem_livros is null then
    dbms_output.put_line('A editora não existe');
  elsif v_tem_livros then
    dbms_output.put_line('A editora tem livros');
  else
    dbms_output.put_line('A editora não tem livros');
  end if;
end;
/

-- EDITORA TEM LIVROS
declare
  v_tem_livros boolean;
begin
  v_tem_livros := fncTemLivrosEditora(1500);
  if v_tem_livros is null then
    dbms_output.put_line('A editora não existe');
  elsif v_tem_livros then
    dbms_output.put_line('A editora tem livros');
  else
    dbms_output.put_line('A editora não tem livros');
  end if;
end;
/

-- EDITORA NAO TEM LIVROS
declare
  v_tem_livros boolean;
begin
  v_tem_livros := fncTemLivrosEditora(1400);
  if v_tem_livros is null then
    dbms_output.put_line('A editora não existe');
  elsif v_tem_livros then
    dbms_output.put_line('A editora tem livros');
  else
    dbms_output.put_line('A editora não tem livros');
  end if;
end;
/

-- 7. Criar um novo script PL/SQL para implementar uma função, designada fncClienteInfo, para retornar a informação pessoal de um
--   dado cliente, recebido por parâmetro. Se o parâmetro fornecido for inválido, a função deve retornar o valor NULL, usando o 
--   mecanismo de exceções. Testar adequadamente a função implementada, através de blocos anónimos.
SELECT * 
FROM CLIENTE;

create or replace function fncClienteInfo(p_nifcliente cliente.nifcliente%type) return cliente%rowtype
is
  v_ret cliente%rowtype;
begin
  select * into v_ret
    from cliente
   where nifcliente = p_nifcliente;
  --
  return v_ret;
exception
  when no_data_found then
    return null;
end;
/

-- CLIENTE
declare
  v_dados cliente%rowtype;
begin
  v_dados := fncClienteInfo('900800100');
  if v_dados.nifCliente is not null then
    dbms_output.put_line('Nome: ' || v_dados.nome);
    dbms_output.put_line('NIF: ' || v_dados.nifCliente);
    dbms_output.put_line('Data nascimento: ' || v_dados.dataNascimento);
    dbms_output.put_line('Telefone: ' || v_dados.nrTelemovel);
    dbms_output.put_line('Codigo Postal: ' || v_dados.codPostal);
    dbms_output.put_line('Morada: ' || v_dados.morada);
  else
    dbms_output.put_line('Cliente não encontrado.');
  end if;
end;
/

-- CLIENTE NAO ENCONTRADO
declare
  v_dados cliente%rowtype;
begin
  v_dados := fncClienteInfo('900800101');
  if v_dados.nifCliente is not null then
    dbms_output.put_line('Nome: ' || v_dados.nome);
    dbms_output.put_line('NIF: ' || v_dados.nifCliente);
    dbms_output.put_line('Data nascimento: ' || v_dados.dataNascimento);
    dbms_output.put_line('Telefone: ' || v_dados.nrTelemovel);
    dbms_output.put_line('Codigo Postal: ' || v_dados.codPostal);
    dbms_output.put_line('Morada: ' || v_dados.morada);
  else
    dbms_output.put_line('Cliente não encontrado.');
  end if;
end;
/

-- 8. Criar um novo script PL/SQL para implementar uma função, designada fncStockAnoEditora, para retornar o stock dos livros 
--   editados por uma dada editora num dado ano. A função deve receber, por parâmetro, o identificador da editora e o ano.
--   Este último parâmetro deve ser opcional na invocação da função e o seu valor por omissão deve ser o ano atual. Se qualquer 
--   parâmetro fornecido for inválido, a função deve retornar o valor NULL, usando o mecanismo de exceções. Testar adequadamente 
--   a função implementada, através de blocos anónimos.
select to_char(sysdate, 'yyyy'), extract(year from sysdate) from dual;

create or replace function fncStockAnoEditora(p_ideditora editora.ideditora%type,
p_ano int default extract(year from sysdate)) return int
is
  v_ret int;
  ex_editora_inexistente exception;
begin
  --Se o parâmetro fornecido for inválido... (considero que ser inválido é a editora não existir)
  select count(*) into v_ret
    from editora
   where ideditora = p_ideditora;
  if v_ret = 0 then
    raise ex_editora_inexistente;
  end if;

  select count(*) into v_ret
    from edicaolivro
   where ideditora = p_ideditora and anoedicao = p_ano;
  --
  return v_ret;
exception
  when ex_editora_inexistente then
    return null;
end;
/

select * from edicaolivro where ideditora = 1500;
--TESTES
SET SERVEROUTPUT ON;
declare
  v_count int;
begin
  v_count := fncStockAnoEditora(1500, 2015);
  if v_count is null then
    dbms_output.put_line('A editora não existe.');
  else
    dbms_output.put_line('O número total de livros da editora é ' || v_count);
  end if;
end;
/

-- EDITORA NAO EXISTE
declare
  v_count int;
begin
  v_count := fncStockAnoEditora(1501, 2015);
  if v_count is null then
    dbms_output.put_line('A editora não existe.');
  else
    dbms_output.put_line('O número total de livros da editora é ' || v_count);
  end if;
end;
/

-- 9. Criar um novo script PL/SQL para implementar uma função, designada fncPrecoVenda, para retornar o preço aplicado a uma dada
--   venda, recebida por parâmetro. Se o parâmetro fornecido for inválido, a função deve retornar o valor NULL, usando o mecanismo
--   de exceções. Testar adequadamente a função implementada, através de blocos anónimos.
select * from venda where nrvenda = 1;
select * from precoedicaolivro where isbn = '500-1234567891';
/*
Notas:
O preço de uma venda é o preço constante na tabela PrecoEdicaoLivro.
Parâmetro inválido é quando o nº da venda não existe.
*/
select * from venda;
select * from precoedicaolivro;
select *
  from venda
 where not exists(select * from precoedicaolivro where isbn = venda.isbn);
 
create or replace function fncPrecoVenda(p_nrvenda venda.nrvenda%type)
return precoedicaolivro.preco%type
is
  v_ret precoedicaolivro.preco%type; 
  v_data venda.datahora%type;
  v_isbn venda.isbn%type;
  v_qtd venda.quantidade%type;
  ex_venda_inexistente exception;
  ex_livro_sem_preco exception;
begin
  begin
    select datahora, isbn, quantidade into v_data, v_isbn, v_qtd
      from venda
     where nrvenda = p_nrvenda;
  exception
    when no_data_found then
      raise ex_venda_inexistente;
  end;
  begin
    select preco into v_ret
      from precoedicaolivro
     where isbn = v_isbn and datainicio <= v_data;
  exception
    when no_data_found then
      raise ex_livro_sem_preco;
  end;
  return v_ret * v_qtd;
exception
  when ex_venda_inexistente then
    return null;
  when ex_livro_sem_preco then
    return null;
end;
/

-- TESTES
SET SERVEROUTPUT ON;

declare
  v_valor number;
begin
  v_valor := fncPrecoVenda(14);
  if v_valor is null then
    dbms_output.put_line('A venda não existe ou o preço não está definido');
  else
    dbms_output.put_line('O valor da venda é ' || v_valor);
  end if;
end;
/

-- VENDA NAO EXISTE
declare
  v_valor number;
begin
  v_valor := fncPrecoVenda(15);
  if v_valor is null then
    dbms_output.put_line('A venda não existe ou o preço não está definido');
  else
    dbms_output.put_line('O valor da venda é ' || v_valor);
  end if;
end;
/

